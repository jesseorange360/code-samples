<?php
class Messages_model extends CI_Model {
	
	//Returns all messages posted by the user with the specified username, most recent first
	public function getMessagesByPoster($name) {
	$query = $this->db->query("SELECT * 
								FROM Messages 
								WHERE user_username = ? 
								ORDER BY posted_at DESC;", 
								$name); 
	//Run SQL Query. Select everything from Messages where the username equals the supplied parameter
	
	return $query->result(); //Return the results of the SQL query
	}
	
	//Returns all messages containing the specified search string, most recent first FIX THIS YOU ASSHOLE
	public function searchMessages($string) {
	$query = $this->db->query("SELECT * 
								FROM Messages 
								WHERE text LIKE '%' ? '%'
								ORDER BY posted_at DESC;", 
								$string);
	//Runs SQL Query. Select everything from Messages where the field text contains the search parameter
		
	if($query->num_rows() > 0) { //If the query returned at least 1 result
		return $query->result(); //Return the results of the SQL query
	}
	}
	
	public function insertMessage($poster, $string) {
	$date = date('Y-m-d H:i:s'); //Store the date format to be used
	//Insert data into messages: the username, the text and the current date 
	$query = $this->db->query("INSERT INTO Messages (user_username, text, posted_at)
								VALUES (?, ?, ?);", 
								array($poster, $string, $date));
	}
	
	//Returns all of the messages from all of those followed by the specified user, most recent first
	public function getFollowedMessages($name) {
	/* Select everything from Messages, join Users on Messages where the username is the same (join Users onto Messages)
	Join User_Follows on Messages where the username equals the followed_username. (Join User_Follows on Messages)
	After this use a where clause to say only get messages where the follower_username is equal to the name variable 
	(get the messages from users that the currently logged in user follows) */
	$query = $this->db->query( "SELECT *  
								FROM Messages 
								JOIN Users ON Messages.user_username = Users.username
								JOIN User_Follows ON Messages.user_username = User_Follows.followed_username
								WHERE follower_username = ? 
								ORDER BY posted_at DESC;", 
								$name);

									
	if($query->num_rows() > 0) { //If the query returned at least 1 result
		return $query->result(); //Return the results of the SQL query
		}
	}
}
?>