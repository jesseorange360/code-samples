<!-- REMEMBER THIS IS FOR ACCESSING DATABASES AND STUFF -->
<?php
class Users_model extends CI_Model {

public function checkLogin($username, $password) {
$encpass = sha1($password); //Store password as a hash value (sha1) called $encpass 
$query = $this->db->query("SELECT * 
							FROM Users 
							WHERE username = ? 
							AND password = ?;", array($username, $encpass));
//Run SQL Query. Select everything from Users where the username and password match the parameters  

if($query -> num_rows() == 1) { //If at least 1 result is found
	return true;
   }
   else
   {
     return false;
   }
}

//Returns TRUE if $follower is following $followed, FALSE otherwise
public function isFollowing($follower,$followed) {
$query = $this->db->query("SELECT *
							FROM User_Follows
							WHERE follower_username = ?
							AND followed_username = ?;", 
							array($follower, $followed));

if($query -> num_rows() == 0) { //If no result is found
	return false;
   }
   else{
	return true;
   }
}

//Inserts a row into the Following table indicating that $follower follows $followed
public function follow($follower,$followed) {
$query = $this->db->query("INSERT INTO User_Follows (follower_username, followed_username)
								VALUES (?, ?);", 
								array($follower,$followed));
//Insert the name of the person following and the person to be followed in the 
								
}

//test
public function register($username, $password) {
$encpass = sha1($password); //Store password as a hash value (sha1) called $encpass 
$query = $this->db->query("INSERT INTO Users (username, encpass)
								VALUES (?, ?);", 
								array($username, $encpass));
}
}
?>