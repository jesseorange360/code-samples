<?php
class Search extends CI_Controller
{
    
    //Displays the Search view
    public function index() 
	{
        $data['username'] = $this->session->userdata('username'); //$data stores the username which was retreived from the session variable
        $this->load->view('search', $data); //Load the search view and pass in the username stored in $data
    }
    
    //Loads Messages_model, retrieves search string from GET parameters, runs searchMessages() and displays the output in the ViewMessages view
    public function dosearch()
    {
        $string = $this->input->get('search'); //Get the input from the search form using GET and store this as $string
        $this->load->model('messages_Model'); //Load the messages_model
        $data['search_result'] = $this->messages_Model->searchMessages($string); //Call method from messages model
        $data['username']      = $this->session->userdata('username'); //$data stores the username which was retreived from the session variable
		$data['following'] = "";
        $this->load->view('viewMessages', $data); //Load the viewMessages model and pass in the result of the query and the username stored in $data
    }
}
?>
