<?php
class User extends CI_Controller
{
		
    //Displays the Login view
    public function login()
    {
        $this->load->view('login'); //Load the Login view
    }
    
    //Logs the user out, clearing their session variable, and redirects to /user/login
    public function logout()
    {
        $this->session->sess_destroy(); //Clear the user's session variable
        redirect("user/login"); //Redirect to login page
    }
	
	public function registerUser()
	{
		$username = $this->input->post('username'); //create a variable to store the POSTed username
        $password = $this->input->post('password'); //create a variable to store the POSTed password
        $this->load->model('users_model'); //Load the users_model
        $this->users_model->register($username, $password); //run register
		
		$query = $this->users_model->checkLogin($username, $password); //run checkLogin
        
        if ($query) { //If the query returned true meaning the credentials matched
            
            $data = array( //array to be inserted into $data
                "username" => $this->input->post("username"), //Set username as the username entered into the log in
                "logged_in" => true //set a variable is logged in to true when logged in
            );
            
            $this->session->set_userdata($data); //Set the session data to the data stored in the above array
            redirect("user/view/$username"); //Redirect to the messages page for the specified user
		}
	}
    
    /* Loads the Users_model, calls checkLogin() passing POSTed user/pass & either re-displays Login view with error message, 
    or redirects to the user/view/{username} controller to view their messages. If login is successful, 
	a session variable containing the username is set. */
    public function doLogin()
    {
        $username = $this->input->post('username'); //create a variable to store the POSTed username
        $password = $this->input->post('password'); //create a variable to store the POSTed password
        $this->load->model('users_model'); //Load the users_model
        $query = $this->users_model->checkLogin($username, $password); //run checkLogin
        
        if ($query) { //If the query returned true meaning the credentials matched
            
            $data = array( //array to be inserted into $data
                "username" => $this->input->post("username"), //Set username as the username entered into the log in
                "logged_in" => true //set a variable is logged in to true when logged in
            );
            
            $this->session->set_userdata($data); //Set the session data to the data stored in the above array
            redirect("user/view/$username"); //Redirect to the messages page for the specified user
			
        } else {
            $this->load->view('login'); //Load the login view			
        }
    }
    
    //Loads Messages_model, runs getMessagesByPoster() passing the specified name, and displays output in the ViewMessages view
    public function view($name = NULL) //Set $name to NULL to start with and then check whether it actually is below
    {
		$follower = $this->session->userdata('username'); //Define follower as the currently logged in user
		$followed = $name; //Define followed as the name of the user currently being viewed
		
		if($name == NULL) { //If name has been left out of the url, instead of an error it will display a message
			echo "Oh dear, you appear to have forgotton to specify a name after <b> user/view/ </b> - Please go and add a username. <br> 
					You can choose: 
					<ul>
						<li>kris</li>
						<li>ben</li>
						<li>hannah</li>
						<li>paul</li> 
						<li>keith</li>
					</ul>";
		}
		else{
			$this->load->model('messages_Model'); //Load messages_model
			$data['search_result'] = $this->messages_Model->getMessagesByPoster($name); //run method in messages model, store result as $data
			$data['username'] = $this->session->userdata('username');
			$this->load->model('users_model'); //Load users_model 
		
		if($this->users_model->isFollowing($follower, $followed) == TRUE) { //If you are not following the currently viewed user
			$data['following'] = 'You are currently following '. $name;
			$this->load->view('viewMessages',$data);
			}
			else{
				if($follower == $followed){ //If the follower (the loggedf in user) is on their own page
					$data['following'] = ''; //Display nothing
					$this->load->view('viewMessages',$data); //Load the viewMessages model, passing in $data so the view can access the results of the query
					}
				else{ //If the currently logged in user is on another users page (who they do not currently follow)
					$data['following'] = 'You are not following '.$name.' <a href="http://jesseorange.hol.es/CI/index.php/user/follow/'.$name.'">Follow this user</a>';
					$this->load->view('viewMessages',$data); //Load the viewMessages model, passing in $data so the view can access the results of the query
					}
				}
			}
	}

    //Loads the Users_model, and invokes the follow() function to add the entry into the database table. Should redirect back to the /user/view/{followed} page when done
    public function follow($followed)
    {
		$follower = $this->session->userdata('username');
        $this->load->model('users_model'); //Load users_model
        $this->users_model->follow($follower, $followed); //Run the method follow() in the users_model
        redirect("user/feed/" . $this->session->userdata('username')); //Redirect to the view function in user (to followed)
    }
    
    //Loads the Messages_model, invokes the getFollowedMessages() function, and puts the output into the ViewMessages view.
    public function feed($name = NULL) //Set $name to NULL to start with and then check whether it actually is below
    {
		if($name == NULL) {
			echo "How can you view a feed when you haven't specified a user?";
		}
		else{
        $this->load->model('messages_model'); //Load users_model
        $data['username'] = $this->session->userdata('username');
        $data['search_result'] = $this->messages_model->getFollowedMessages($name); //run the getFollowedMessages method
		$data['following'] = ""; //Set following to an empty string so that it can be found byr does not output anything
        $this->load->view('viewMessages', $data); //Load the viewMessages model, passing in the information stored in data
		}
	}
}
?> 
