<?php
class Message extends CI_Controller {
	
	//Displays the Post view
	public function index() 
	{
		if($this->session->userdata('logged_in') == false) { //if no one is logged in
		redirect("user/login"); //redirect to login page
		}
	else{
		$data['username'] = $this->session->userdata('username'); //$data stores the username which was retreived from the session variable
		$this->load->view('post', $data); //Load the search view and pass in the username stored in $data
		}
	}

	/* Redirects to /user/login if not logged in. Loads the Messages_model, runs the insertMessage function 
	passing the currently logged in user from your session variable, along with the posted message.
	Redirects to /user/view/{username} when done, which should show the user’s new post */
	public function doPost($post) 
	{
		if($this->session->userdata('logged_in') == false) { //if no one is logged in
			redirect("user/login"); //redirect to login page
		}
		else{
			$poster = $this->session->userdata('username'); //set $poster to the username stored in the session variable
			$string = $this->input->post('post'); //set $string to the text typed in the form submitted via POST
			$this->load->model('messages_Model'); //Load the messages_model
			$this->messages_Model->insertMessage($poster, $string); //Call insertMessage from messages Model and pass in $poster and $string
			redirect("user/view/" . $this->session->userdata('username')); //add the username to the end of the URL by concationating the session variable
		}
	}
}
?>
