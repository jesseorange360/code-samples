<!DOCTYPE html>
<html>
<head>
<title>View your messages</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url("media/style.css"); ?>"/> <!-- uses base_url followed by path to CSS file -->
</head>
<body>
<div id="container">

	<div id="header">
		<h1>Messages</h1>
			<div id="headertext">
			<?php
			if($this->session->userdata('logged_in') == true) {
				echo "You are logged in as: <span id='loggedinas'> $username </span>";
				}
				else{
					echo "You are not logged in <a href='http://jesseorange.hol.es/CI/index.php/user/login'>Login</a>";
					}
			?>
			</div>
	</div>
	
	<div id="navigation">
	
		<ul>
			<li><a href="<?php echo site_url("user/view/$username"); ?>">My Messages</a></li>
			<li><a href="<?php echo site_url("search"); ?>">Search</a></li>
			<li><a href="<?php echo site_url("message"); ?>">Post Message</a></li>
			<li><a href="<?php echo site_url("user/feed/$username"); ?>">Feed</a></li>
			<li><a href="<?php echo site_url("user/logout"); ?>">Logout</a></li>
		</ul>
	
	</div>
	
	<div id="main">
		
		<?php
		
			echo $following;
			 
			if($search_result == 0) { //If the query returned no results
				echo "<span id='error'> Sorry your search returned no results, go and search for something else </span>";
			}
		
			else{ //If results were found
		
			foreach($search_result as $row) //Loop through search_results as $row
			{
				echo "<div id ='name'>";
				echo $row->user_username;
				echo "</div>";
				echo "<div id ='message'>";
				echo $row->text;
				echo "</div>";
				echo "<div id ='time'>";
				echo $row->posted_at;
				echo "</div>";
			}
		}
		
		?>

	</div>

	<div id="footer">
		<p>Created by Jesse Orange - University of Kent</p>
	</div>
</body>
</html>