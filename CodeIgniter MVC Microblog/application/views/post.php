<!--Displays a form in which the user can write a new post. This form should submit via POST to the /message/doPost action --> 

<!DOCTYPE html>
<html>
<head>
<title>Post a Message</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url("media/style.css"); ?>"> <!-- uses base_url followed by path to CSS file -->
</head>
<body>
<div id="container">
	<div id="header">
		<h1>Post Something</h1>
		<div id="headertext">
			<?php
			if($this->session->userdata('logged_in') == true) {
				echo "You are logged in as: <span id='loggedinas'> $username </span>";
				}
				else{
					echo "You are not logged in";
					}
				?>
		</div>
	</div>
	
	<div id="navigation">
	
		<ul>
			<li><a href="<?php echo site_url("user/view/$username"); ?>">My Messages</a></li> <!-- uses site_url followed by path to page -->
			<li><a href="<?php echo site_url("search"); ?>">Search</a></li>
			<li><a href="<?php echo site_url("message"); ?>">Post Message</a></li>
			<li><a href="<?php echo site_url("user/feed/$username"); ?>">Feed</a></li>
			<li><a href="<?php echo site_url("user/logout"); ?>">Logout</a></li>
		</ul>
	
	</div>
	
	<div id="main">
	
		<div id="formcontainer">
	
			<form id="postform" action="http://jesseorange.hol.es/CI/index.php/message/doPost" method="POST">
			<label for="post">Post Message!</label> <br>
			<textarea name="post" placeholder="Write a Message"></textarea> <br>
			<input type="submit" id="submit" value="Post Message">
			</form>
		
		</div>

	</div>
	
	<div id="footer">
		<p>Created by Jesse Orange - University of Kent</p>
	</div>
</div>
</body>
</html>