 <!DOCTYPE html>
<html>
<head>
<title>Search</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url("media/style.css"); ?>"> <!-- uses base_url followed by path to CSS file -->
</head>
<body>
<div id="container">
	<div id="header">
		<h1>Search the Orange Room</h1>
			<div id="headertext">
				<?php
					if($this->session->userdata('logged_in') == true) {
						echo "You are logged in as: <span id='loggedinas'> $username </span>";
					}
					else{
						echo "You are not logged in <a href='http://jesseorange.hol.es/CI/index.php/user/login'>Login</a>";
						}
				?>
			</div>
	</div>
	
	<div id="navigation">
	
		<ul>
			<li><a href="<?php echo site_url("user/view/$username"); ?>">My Messages</a></li>
			<li><a href="<?php echo site_url("search"); ?>">Search</a></li>
			<li><a href="<?php echo site_url("message"); ?>">Post Message</a></li>
			<li><a href="<?php echo site_url("user/feed/$username"); ?>">Feed</a></li>
			<li><a href="<?php echo site_url("user/logout"); ?>">Logout</a></li>
		</ul>
	
	</div>
	
	<div id="main">
		
		<div id="formcontainer">
		
			<form id="searchform" action="http://raptor.kent.ac.uk/~jlo21/CI/index.php/search/dosearch" method="GET">
			<label for="search">Search in the box</label> <input type="text" name="search" placeholder="Enter Search terms"><br>
			<input type="submit" id="submit" value="Search">
			</form>
		
		</div>	

	</div>
	
	<div id="footer">
		<p>Created by Jesse Orange - University of Kent</p>
	</div>
</div>
</body>
</html>