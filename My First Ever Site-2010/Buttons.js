function displayDate(){
	var currentDate = new Date()
	var month = currentDate.getMonth() + 1
	var day = currentDate.getDate()
	var year = currentDate.getFullYear()
	document.write(day + "/" + month + "/" + year)
	return true
}
function displayTime(){
	var currentTime = new Date()
	var hours = currentTime.getHours()
	var minutes = currentTime.getMinutes()
	if (minutes < 10){
		minutes = "0" + minutes
	}
	document.write("\n" + hours + ":" + minutes + " ")
	if(hours > 11){
		document.write("PM")
	} else {
		document.write("AM")
	}
	return true
}
