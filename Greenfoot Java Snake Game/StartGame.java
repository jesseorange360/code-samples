import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * StartGame World - Acts as the main menu
 * 
 * @author Jesse Orange
 * @version V1 
 */
public class StartGame extends World
{
   public StartGame()
    {    
        // Create a new world with 300x300 cells with a cell size of 1x1 pixels.
        super(30, 30, 10);
        populate();//add objects the the world.
    }
    
   void populate()
    {
        // Add Start Button
        addObject(new Start(),15,10);
        // Add Instructions button
        addObject(new Instructions(),15,14);
        // Add Food Actor
        addObject(new Food(),1,6);
        addObject(new Food(),3,6);
        addObject(new Food(),5,6);
        addObject(new Food(),7,6);
        addObject(new Food(),9,6);
        addObject(new Food(),11,6);
        addObject(new Food(),13,6);
        addObject(new Food(),15,6);
        addObject(new Food(),17,6);
        addObject(new Food(),19,6);
        addObject(new Food(),21,6);
        addObject(new Food(),23,6);
        addObject(new Food(),25,6);
        addObject(new Food(),27,6);
                
   }
}
