import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * An Actor thats a Game Over sign.
 * 
 * @author Jesse Orange
 * @version V1
 */
public class GameOver extends Actor
{
    public void act() 
    {
        //Act - Do whatever
    }    
}
