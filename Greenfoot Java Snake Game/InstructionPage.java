import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * InstructionsPage is a world which shows the instructions for the game.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class InstructionPage extends World
{
    public InstructionPage()
    {    
        // Create a new world with 300x300 cells with a cell size of 1x1 pixels.
        super(30, 30, 10);
        populate();//add objects to the world.
    }
    
   void populate()
    {
        // Add Home button Actor.
        addObject(new Home(),15,26);
   }
}
