import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List; // import java for lists

/**
 * This actor is the one that follows the SnakeHead and acts as the body of the Snake.
 * 
 * @author Jesse Orange
 * @version V1 
 */
public class SnakeBody  extends Actor
{
    public int offsetX = 0; //global variable storing the random X direction (0)
    public int offsetY = 0; //global variable storing the random Y direction (0)
    public SnakeBody lastSnakeBody=null; //there is no last SnakeBody
    public SnakeHead lastSnakeHead=null; //there is no last SnakeHead
    public int lastx; //variable for storing the last x co-ordinate
    public int lasty; //variable for storing the last x co-ordinate
    boolean joinedtoHead; // a check to see whether the body is joined to the head
    SnakeBody nextSnakeBody;
    
    public SnakeBody (Actor thisBody, boolean isSnakeHead) //check to see whether the current segment is a SnakeHead or a SnakeBody 
    {
      if(isSnakeHead) //if the current part is a SnakeHead add the SnakeBody at the last co-ordinates of the SnakeHead  
      {
          lastSnakeHead=(SnakeHead)thisBody;
          joinedtoHead=isSnakeHead;
          setLocation(lastSnakeHead.lastx, lastSnakeHead.lasty);
        }
      else // add a SnakeBody to the last co-ordinates of the previous SnakeBody
      {
      lastSnakeBody=(SnakeBody)thisBody;
      setLocation(lastSnakeBody.lastx, lastSnakeBody.lasty);
        }
    }
  
    public void act() 
    {
        lastx=getX(); //get the last x co-ordinate
        lasty=getY(); //get the last y co-ordinate
        growSnake(); //run the growSnake function.
        move(); //run the move function.
    }    
    

         /**
 * This function is used to get the current actor and set an appropriate location. This is to ensure the next actor follows the head. 
 * 
 */
    private void growSnake() //the SnakeBody will follow behind the head by using the current x and y of the head 
    {
         int currX = getX(); //get current x co-ordinate
         int currY = getY(); // get current y co-ordinate 
         
         List getSnakeHead=getWorld().getObjects(SnakeHead.class); //get the SnakeHead actor from the world.
         if(!getSnakeHead.isEmpty  ()) //if there is no SnakeHead
         {
            SnakeHead thisSnakeHead=(SnakeHead)getSnakeHead.get(0); //is there a SnakeHead in the current location?
            offsetX=thisSnakeHead.offsetX; //the x co-ordinate of SnakeHead
            offsetY=thisSnakeHead.offsetY; //the y co-ordinate of SnakeHead
        }
        setLocation(currX+offsetX, currY+offsetY); //
    }
    
      /**
 * This function checks whether a body segment is joined to the Snake's body or its head. After this has been done a location is set using the lask known co-ordinate
 * of either the SnakeHead or SnakeBody so that it can follow behind the current segment.
 * 
 */
     private void move() //
    {
         if(joinedtoHead)
         {
             setLocation(lastSnakeHead.lastx, lastSnakeHead.lasty);
            }
            else
            {
                setLocation(lastSnakeBody.lastx, lastSnakeBody.lasty);
            }
         }
    
    /**
 * This function adds a SnakeBody actor. The function says if there is no SnakeBody make the next actor a SnakeBody. It then says that if there is not a SnakeBody add
 * one at the last co-ordinates of the SnakeHead. This function makes sure the correct actor is added in the correct place.
 */
    public void addSnakeBody()
    {
        if(nextSnakeBody==null){ //if there is no SnakeBody
            int currX = getX(); //get current x co-ordinate
            int currY = getY(); // get current y co-ordinate 
            nextSnakeBody=new SnakeBody((Actor)this, false); //newSnakeBody is a SnakeBody actor
            if(nextSnakeBody!=null)
                getWorld().addObject(nextSnakeBody, currX-offsetX, currY-offsetY); //add SnakeBody at location of SnakeHead - 1
        }
         else nextSnakeBody.addSnakeBody(); // if there is a SnakeBody add another   
         }
    
      /**
 * This function sets whether an actor can move by giving variables and asking whether these are true or false. If there is an object in the current co-ordinate an
 * actor cannot move. False means an actor found and object in the list and will stop moving. True means the actor found none of these objects and will move freely.
 */
      boolean canMove(int x,int y)     
        {
        List foundWall = getWorld().getObjectsAt(x,y, Wall.class); //canMove is false if a Wall is found
        if(!foundWall.isEmpty  ())return false;   //if Wall found return false and stop otherwise continue
       
        List foundSnakeHead = getWorld().getObjectsAt(x,y, SnakeHead.class); //canMove is false if a SnakeHead is found
        if(!foundSnakeHead.isEmpty  ())return false;   //if SnakeHead found return false and stop otherwise continue
        
        List foundSnakeBody = getWorld().getObjectsAt(x,y, SnakeBody.class); //canMove is false if a SnakeBody is found
        if(!foundSnakeBody.isEmpty  ())return false;   //if SnakeBody found return false and stop otherwise continue
        
        return true;    //this must be at the end – if canMove got this far it didn't find any of the listed objects.    
    }
}

