import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List; // import java for lists

/**
 * The SnakeHead is the main actor in my game and controls nearly everything else as everything happens through this actor. 
 * This actor is the leader in the chain when the Snake is created and controls multiple aspects.
 * @author Jesse Orange
 * @version V1
 */
public class SnakeHead extends Actor
{

  public int offsetX = 0; //global variable storing the random X direction (1, 0 or -1)
  public int offsetY = -1; //global variable storing the random Y direction (1, 0 or -1)
  public int lastx; //variable for storing the last x co-ordinate
  public int lasty; //variable for storing the last x co-ordinate
  public SnakeBody nextSnakeBody=null; //there is no next SnakeBody at the beginning
  public SnakeHead nextSnakeHead=null; //there is no next SnakeHead at the beginning
  int currImage; //0 will represent Princess
 
  /**
 * A constructer to set a direction for the Snake.
 */
  public SnakeHead() // constructor
   {
       setDirection(); //set a direction for the Snake
       currImage=0; //set the current image to 0. There are two images (1 and 0).
       setImage("SnakeHead.png"); //set a specific image.
   }  
 
   public void act()
   {
        lastx = getX(); //get current x co-ordinate
        lasty = getY(); // get current y co-ordinate
        processKeys(); //run the processKeys function
        move(); //run the move function
        setDirection(); //run the setDirection function
        changeImage(); //Run the change image function
        findEatFood(); //run the findEatFood function
    }
    
    /**
 * This function checks to see if a specific key has been pressed using an if statement then applies the offsets within each statement to move the Snake in a direction.
 */
   private void processKeys()
   {
     if(Greenfoot.isKeyDown("left")) //move left
     {
         offsetX=-1;
         offsetY=0;
        }
     if(Greenfoot.isKeyDown("right")) //move right
     {
         offsetX=1;
         offsetY=0;
        }
     if(Greenfoot.isKeyDown("up")) //move up
     {
         offsetX=0;
         offsetY=-1;
        }
     if(Greenfoot.isKeyDown("down")) //move down
     {
         offsetX=-0;
         offsetY=1;
        }
    }
    
    
    /**
 * This function checks to see whether an actor can actually move using the current co-ordinates and direction. This function uses the CanMove function in order to work.
 * If the Snake cannot move the GameOver actor will appear and Greenfoot will stop.
 */
   private void move() //
    {
     int currX = getX(); //get current x co-ordinate
     int currY = getY(); // get current y co-ordinate
     if(canMove(currX + offsetX, currY + offsetY))
     setLocation(currX + offsetX, currY + offsetY); //if the snake can move it will move in a direction
     else
     { 
         getWorld().addObject(new GameOver(),15,15); //add the GameOver Actor
         Greenfoot.playSound("Smash.mp3"); //play sound file
         Greenfoot.stop(); //Stop Greenfoot
        }
    }
    
    /**
 * This function sets the current direction of the Snake using the x and y offsets. The direction is changed using a rotation of a certain degrees.
 */
     private void setDirection() //method to set the current direction
    {
        if(offsetX == -1 && offsetY == 0) //if the snake will go west
            setRotation(270); //set rotation to west
        else if(offsetX == 0 && offsetY == -1) //if the snake will go north
            setRotation(0); //set rotation to north
        else if(offsetX == 0 && offsetY == 1) //if the snake will go south
            setRotation(180); //set rotation to south
        else if(offsetX == 1 && offsetY == 0) //if the snake will go east
            setRotation(90); //set rotation to east
    }
    
    /**
 * This function looks to see whether there is an object at the current co-ordinate, if there is, the object is removed from the world and the score is updated.
 * Once the score has been updated add another object in a random location on the grid and then add a new SnakeBody using the function addSnakeBody.
 */
    private void findEatFood()// funcion to eat an actor. Specifically Food
       {
      Actor thisFood=getOneObjectAtOffset(0,0,Food.class); // is there Food in the current square?
      if(thisFood!=null) //there is no Food
      {
          Greenfoot.playSound("Belch.wav"); //play sound file
          getWorld().removeObject(thisFood); //removes an actor from the world once it has been eaten.
          List getScore=getWorld().getObjects(Score.class); //get the score
      if(!getScore.isEmpty())
      {
         Score currScore=(Score)getScore.get(0); // check that there is a score
         currScore.updateScore(1); //add 1 to the current score
        }    
        
      int x = Greenfoot.getRandomNumber(26)+1; //random x co-ordinate
      int y = Greenfoot.getRandomNumber(26)+1; //random y co-ordinate
      getWorld().addObject(new Food(), x, y); //add new object at a random co-ordinate
      
      addSnakeBody(); //addSnakeBody actor
      }
    }
    
    /**
 * This function adds a SnakeBody actor. The function says if there is no SnakeBody make the next actor a SnakeBody. It then says that if there is not a SnakeBody add
 * one at the last co-ordinates of the SnakeHead. This function makes sure the correct actor is added in the correct place.
 */
    private void addSnakeBody() //
    {
        if(nextSnakeBody==null){ //if there is no SnakeBody
            int currX = getX(); //get current x co-ordinate
            int currY = getY(); // get current y co-ordinate 
            nextSnakeBody=new SnakeBody((Actor)this, true); //newSnakeBody is a SnakeBody actor
            if(nextSnakeBody!=null)
                getWorld().addObject(nextSnakeBody, lastx, lasty); //add SnakeBody at the last location of SnakeHead
        }
         else nextSnakeBody.addSnakeBody(); // if there is a SnakeBody add another 
         }
    
   
     /**
 * This function sets whether an actor can move by giving variables and asking whether these are true or false. If there is an object in the current co-ordinate an
 * actor cannot move. False means an actor found and object in the list and will stop moving. True means the actor found none of these objects and will move freely.
 */
     boolean canMove(int x,int y)     
        {
        List foundWall = getWorld().getObjectsAt(x,y, Wall.class); //canMove is false if a Wall is found
        if(!foundWall.isEmpty  ())return false;   //if Wall found return false and stop otherwise continue
        
        List foundSnakeHead = getWorld().getObjectsAt(x,y, SnakeHead.class); //canMove is false if a SnakeHead is found
        if(!foundSnakeHead.isEmpty  ())return false;   //if SnakeHead found return false and stop otherwise continue
        
        List foundSnakeBody = getWorld().getObjectsAt(x,y, SnakeBody.class); //canMove is false if a SnakeBody is found
        if(!foundSnakeBody.isEmpty  ())return false;   //if SnakeBody found return false and stop otherwise continue
       
        return true;    //this must be at the end – if canMove got this far it didn't find any of the listed objects.
    }

      /**
 * This function sets the current image using the variables 0 and 1. Each number has a seperate image and the number changes each time the SnakeHead moves in order to
 * create an animated actor.
 */
    private void changeImage()
   {
       if(currImage==0)
       {
           setImage("SnakeHead.png");
           currImage=1;
        }
        else if(currImage==1)
        {
          setImage("SnakeHead2.png");
          currImage=0;
        }
            
}
}
