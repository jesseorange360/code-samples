import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * An actor that acts as a button so you can get from the menu to the Platform world.
 * 
 * @author Jesse Orange
 * @version V1
 */
public class Start extends Actor
{
    public void act() 
    {
        if(Greenfoot.mousePressed(this)){ // if the mouse is clicked.
            Greenfoot.setWorld(new Platform()); // go to world Platform
        }
    }
}
