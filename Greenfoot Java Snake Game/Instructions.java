import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Instructions is an Actor that is used as a button in the main menu to take you to another world.
 * 
 * @author Jesse Orange 
 * @version V1
 */
public class Instructions extends Actor
{
    /**
     * Act - do whatever the Instructions wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
   public void act() 
    {
        if(Greenfoot.mousePressed(this)){ //If the mouse has been clicked
            Greenfoot.setWorld(new InstructionPage()); //go to the world "InstructionsPage"
        }
    }
}
