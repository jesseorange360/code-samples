import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.*;

/**
 * Score is an actor which holds a varable for the Score and is updated when the update score method is used
 * with a function in another actor, in this case FindEatFood in SnakeHead.
 * @author Jesse Orange
 * @version V1
 */
public class Score  extends Actor
    {
    int currentScore=0; //this is the global variable
    
    public Score() //this is the constructor
    {
        updateScore(0); //display current score, adding 0 points
    }

    public void act() 
    {
    }
    
    
    public void updateScore(int pointstoAdd)
    {
    currentScore=currentScore+pointstoAdd; //
    setImage("Score.png"); //set the Score Image.
    GreenfootImage image=getImage();
    image.clear(); //clear the current image
    
    Font scoreFont=new Font("Arial",1,12); //set font to arial bold, size 12
    image.setFont(scoreFont);
    image.setColor(new Color(255,255,255)); //set font colour to white
    image.drawString("Score " +currentScore,1,10); //draw the text for the score box.
   }
}

