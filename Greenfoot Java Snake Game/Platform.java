import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Platform - This world contains the main game and is the one that contains the Actors for the game 
 * 
 * @author Jesse Orange
 * @version V1
 */
public class Platform extends World
{

    GreenfootSound backgroundMusic = new GreenfootSound("Frogger.mp3"); //constructor for background music.

    public Platform()
    {    
        // Create a new world with 300x300 cells with a cell size of 1x1 pixels.
        super(30, 30, 10);
        Greenfoot.setSpeed(35); //sets the speed of the game (0-100).
        populate();//add objects the the Platform
        setPaintOrder(GameOver.class, Score.class, SnakeBody.class, SnakeHead.class, Food.class); // sets order in which the objects stack.
        
    }
    
    //public void stopped()  
    {  
         backgroundMusic.pause();  
    }  
  
   // public void started()  
    {  
        backgroundMusic.playLoop();  
    }  
    
    void populate()
    {
        // Add wall down left hand side.
        
        addObject(new Wall(),0,0);
        addObject(new Wall(),0,1);
        addObject(new Wall(),0,2);
        addObject(new Wall(),0,3);
        addObject(new Wall(),0,4);
        addObject(new Wall(),0,5);
        addObject(new Wall(),0,6);
        addObject(new Wall(),0,7);
        addObject(new Wall(),0,8);
        addObject(new Wall(),0,9);
        addObject(new Wall(),0,10);
        addObject(new Wall(),0,11);
        addObject(new Wall(),0,12);
        addObject(new Wall(),0,13);
        addObject(new Wall(),0,14);
        addObject(new Wall(),0,15);
        addObject(new Wall(),0,16);
        addObject(new Wall(),0,17);
        addObject(new Wall(),0,18);
        addObject(new Wall(),0,19);
        addObject(new Wall(),0,20);
        addObject(new Wall(),0,21);
        addObject(new Wall(),0,22);
        addObject(new Wall(),0,23);
        addObject(new Wall(),0,24);
        addObject(new Wall(),0,25);
        addObject(new Wall(),0,26);
        addObject(new Wall(),0,27);
        addObject(new Wall(),0,28);
        addObject(new Wall(),0,29);
                
        //Add wall along top.
        addObject(new Wall(),1,0);
        addObject(new Wall(),2,0);
        addObject(new Wall(),3,0);
        addObject(new Wall(),4,0);
        addObject(new Wall(),5,0);
        addObject(new Wall(),6,0);
        addObject(new Wall(),7,0);
        addObject(new Wall(),8,0);
        addObject(new Wall(),9,0);
        addObject(new Wall(),10,0);
        addObject(new Wall(),11,0);
        addObject(new Wall(),12,0);
        addObject(new Wall(),13,0);
        addObject(new Wall(),14,0);
        addObject(new Wall(),15,0);
        addObject(new Wall(),16,0);
        addObject(new Wall(),17,0);
        addObject(new Wall(),18,0);
        addObject(new Wall(),19,0);
        addObject(new Wall(),20,0);
        addObject(new Wall(),21,0);
        addObject(new Wall(),22,0);
        addObject(new Wall(),23,0);
        addObject(new Wall(),24,0);
        addObject(new Wall(),25,0);
        addObject(new Wall(),26,0);
        addObject(new Wall(),27,0);
        addObject(new Wall(),28,0);
        addObject(new Wall(),29,0);
               
        //Add right hand wall.
        addObject(new Wall(),29,0);
        addObject(new Wall(),29,1);
        addObject(new Wall(),29,2);
        addObject(new Wall(),29,3);
        addObject(new Wall(),29,4);
        addObject(new Wall(),29,5);
        addObject(new Wall(),29,6);
        addObject(new Wall(),29,7);
        addObject(new Wall(),29,8);
        addObject(new Wall(),29,9);
        addObject(new Wall(),29,10);
        addObject(new Wall(),29,11);
        addObject(new Wall(),29,12);
        addObject(new Wall(),29,13);
        addObject(new Wall(),29,14);
        addObject(new Wall(),29,15);
        addObject(new Wall(),29,16);
        addObject(new Wall(),29,17);
        addObject(new Wall(),29,18);
        addObject(new Wall(),29,19);
        addObject(new Wall(),29,20);
        addObject(new Wall(),29,21);
        addObject(new Wall(),29,22);
        addObject(new Wall(),29,23);
        addObject(new Wall(),29,24);
        addObject(new Wall(),29,25);
        addObject(new Wall(),29,26);
        addObject(new Wall(),29,27);
        addObject(new Wall(),29,28);
        addObject(new Wall(),29,29);
                
        // Add bottom wall.
        addObject(new Wall(),0,29);
        addObject(new Wall(),1,29);
        addObject(new Wall(),2,29);
        addObject(new Wall(),3,29);
        addObject(new Wall(),4,29);
        addObject(new Wall(),5,29);
        addObject(new Wall(),6,29);
        addObject(new Wall(),7,29);
        addObject(new Wall(),8,29);
        addObject(new Wall(),9,29);
        addObject(new Wall(),10,29);
        addObject(new Wall(),11,29);
        addObject(new Wall(),12,29);
        addObject(new Wall(),13,29);
        addObject(new Wall(),14,29);
        addObject(new Wall(),15,29);
        addObject(new Wall(),16,29);
        addObject(new Wall(),17,29);
        addObject(new Wall(),18,29);
        addObject(new Wall(),19,29);
        addObject(new Wall(),20,29);
        addObject(new Wall(),21,29);
        addObject(new Wall(),22,29);
        addObject(new Wall(),23,29);
        addObject(new Wall(),24,29);
        addObject(new Wall(),25,29);
        addObject(new Wall(),26,29);
        addObject(new Wall(),27,29);
        addObject(new Wall(),28,29);
        addObject(new Wall(),29,29);
        
        // Add SnakeHead Actor
        addObject(new SnakeHead(),11,11);
        
        // Add Food Actor.
   int x,y; // this code generates an actor in a random location using the variables x and y
       do{
            x = Greenfoot.getRandomNumber(27)+1;
            y = Greenfoot.getRandomNumber(27)+1;
        }while(!getObjectsAt(x, y, null).isEmpty()); //if there are none
        addObject(new Food(), x, y);
        
        // Add Score Actor
        addObject(new Score(),15,2);
    }
}

