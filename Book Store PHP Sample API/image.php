<?php

/**
* image.php allows anyone to view the cover image associated with a book by entering the book id.
* Session checking not required as anyone is free to view images.
**/

require_once __DIR__ . '/config.php';

// If the book_id is empty, inform the user
if (empty($_GET['book_id']))
{
	echo "No ID provided";
}
else // If the field was not empty pass the form variables to the script using GET
{
	$book_id = $_GET['book_id'];

	try
	{
		// Create a new PDO connection
		$conn = new PDO('mysql:host=' . DB_HOST . '; dbname=' . DB_DATABASE, DB_USERNAME, DB_PASSWORD);

		// Set the PDO error mode to exception
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		// Construct SQL
		$sql = "SELECT * FROM book WHERE book_id = :book_id";

		// Prepare sql and bind parameters
		$stmt = $conn->prepare($sql);
		$stmt->bindParam(':book_id', $book_id);
		$stmt->execute();
		
		// If the SQL affected a row
		if ($stmt->rowCount() > 0)
		{
			// Loop into the result and retrieve the path to the image from the database
			foreach($stmt as $row)
			{
				$image_path = $row['image_url'];
			}

			// Present the image to the user by setting the header, then getting the contents of the image
			header("Content-type: image/jpeg");
			$image = file_get_contents($image_path);
			echo $image;
		}
		else // If the SQL didn't return anything
		{
			// Return a 404 response as we couldn't find anything
			http_response_code(404);
			header('HTTP/1.0 404 Not Found');
			echo "<h1>404 Not Found</h1>";
			echo "The page that you have requested could not be found.";
			exit();
		}
	}

	// If the SQL should go wrong, just catch it and echo the error 
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}

	$conn = null;
}

?>