
Information in order to use API service
========================================

Admin users:

	username: jlo21 
	password: Belkin1993j
	email: jlo21@kent.ac.uk
	 
Non admin users:

	username: jo274
	password: WGACA!93j
	email: jo274@kent.ac.uk

PayPal Account Login:

	email: jlo21@kent.ac.uk 
	password: Belkin1993j
	
PayPal Buyer Account

	email: jlo21-buyer@kent.ac.uk
	password: Belkin1993j

Please also note the following:
========================================

- ecommerce_tables.sql contains the SQL dump from generating create table statements in MySQL Workbench.
- JSON output was formed using arrays and by passing JSON_PRETTY_PRINT, to maximise readability.
- Any functions not found in main files are in functions.php, including adding to the audit log,
  this was to reduce code duplicate where possible.
- References to other sources are included in the source files (where necessary).
- purchases returns nothing on no results, as returning "No results" was causing the automated tests to fail.
