<?php

/**
* book_create allows admin users to create a new book entery in the database by filling in information about a book.
* This script also uploads given files to the data folder, given in config.php.
**/

// Start the session and tell the script we require some files
session_start();
require_once __DIR__ . '/config.php';

// If the type variable is empty, then we can presume no one is logged in
if (empty($_SESSION['type']))
	{
	$success = false;
	$message = "You do not appear to be logged in.";
	header('Content-type: application/json');
	$array = array(
		"success" => $success,
		"message" => $message
	);
	$json = json_encode($array, JSON_PRETTY_PRINT);
	echo $json;
	}
	// If the type does not equal admin, stop here, as only admins can create books
  else
if ($_SESSION['type'] != "admin")
	{
	$success = false;
	$message = "Only admin users can access this page.";
	header('Content-type: application/json');
	$array = array(
		"success" => $success,
		"message" => $message
	);
	$json = json_encode($array, JSON_PRETTY_PRINT);
	echo $json;
	}
  else // If we know that an admin is logged in
	{
	// Chect whether the form fields were filled in, uses $_FILES to see if files were added
	if (empty($_POST["title"]) || empty($_POST["authors"]) || empty($_POST["description"]) || empty($_POST["price"]) || empty($_FILES))
		{
		$success = false;
		$message = "Book creation failed";
		header('Content-type: application/json');
		$array = array(
			"success" => $success,
			"message" => $message
		);
		$json = json_encode($array, JSON_PRETTY_PRINT);
		echo $json;
		}
	  else // If all fields were filled in, fill in necessary variables
		{
		// The variables that were directly entered by the user
		$title = $_POST['title'];
		$authors = $_POST['authors'];
		$description = $_POST['description'];
		$price = $_POST['price'];
		
		// The given file names for the files used in the form
		$image = $_FILES['image']['name'];
		$content = $_FILES['content']['name'];
		
		// The temporary names assigned by PHP during upload attempt
		$imgTemp = $_FILES['image']['tmp_name'];
		$contentTemp = $_FILES['content']['tmp_name'];
		
		// The path to where images and other files will be stored
		$imagePath = DATA_FOLDER . $image;
		$contentPath = DATA_FOLDER . $content;
		
		// Generating a unique ID, including the word book
		$book_id = uniqid("book", false);
		
		// Returns the file extension (type) for files to be uploaded
		$image_type = $_FILES['image']['type']; 
		$content_type = $_FILES['content']['type']; 
		
		// Arrays to define the allowed file types, arrays used for potential extendability in future
		$allowed_image_type = array(
			"image/jpeg"
		);
		$allowed_content_type = array(
			"application/pdf"
		);
		
		// If the files uploaded have the wrong file type, stop here and inform the user
		if (!in_array($image_type, $allowed_image_type) || !in_array($content_type, $allowed_content_type))
			{
			$success = false;
			$message = "Images must be of type JPEG and content must be a PDF";
			$book_id = NULL;
			header('Content-type: application/json');
			$array = array(
				"success" => $success,
				"message" => $message,
				"book_id" => $book_id
			);
			$json = json_encode($array, JSON_PRETTY_PRINT);
			echo $json;
			}
		  else // If everything was fine attempt to upload files and add a database entry
		  
		  // Use move_uploaded_file to move content from temporary storage to actual location
		if (move_uploaded_file($imgTemp, $imagePath) && (move_uploaded_file($contentTemp, $contentPath)))
			{
			try
			{
				// Create a new PDO connection
				$conn = new PDO('mysql:host=' . DB_HOST . '; dbname=' . DB_DATABASE, DB_USERNAME, DB_PASSWORD);

				// Set the PDO error mode to exception
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

				// Construct the SQL to add a book to the database
				$sql = "INSERT INTO book (book_id, title, authors, description, price, image_url, content_url)
						VALUES (:book_id, :title, :authors, :description, :price, :image_url, :content_url)";

				// Prepare the SQL and bind parameters
				$stmt = $conn->prepare($sql);
				$stmt->bindParam(':book_id', $book_id);
				$stmt->bindParam(':title', $title);
				$stmt->bindParam(':authors', $authors);
				$stmt->bindParam(':description', $description);
				$stmt->bindParam(':price', $price);
				$stmt->bindParam(':image_url', $imagePath);
				$stmt->bindParam(':content_url', $contentPath);
				$stmt->execute();
				
				// If the statement affected the database
				if ($stmt->rowCount() > 0)
					{ 
					$success = true;
					$message = "The book " . $title . " was created successfully.";
					$book_id = $book_id;
					header('Content-type: application/json');
					$array = array(
						"success" => $success,
						"message" => $message,
						"book_id" => $book_id
					);
					$json = json_encode($array, JSON_PRETTY_PRINT);
					echo $json;
					}
				  else // If something went wrong
					{
					$success = false;
					$message = "Book creation failed";
					$book_id = NULL;
					header('Content-type: application/json');
					$array = array(
						"success" => $success,
						"message" => $message,
						"book_id" => $book_id
					);
					$json = json_encode($array, JSON_PRETTY_PRINT);
					echo $json;
					}
				}

			catch(PDOException $e)
				{
				echo "Error: " . $e->getMessage();
				}

			$conn = null;
			}
		  else // If the files were not uploaded successfully
			{
			$success = false;
			$message = "Book creation failed: this most likely occured due to file size limitations";
			$book_id = NULL;
			header('Content-type: application/json');
			$array = array(
				"success" => $success,
				"message" => $message
			);
			$json = json_encode($array, JSON_PRETTY_PRINT);
			echo $json;
			}
		}
	}

?>
