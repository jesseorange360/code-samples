<?php

/**
 * Allows people to view details of a specific book as well as see the review IDs for the reviews on this particular book.
 * Some books may have no reviews, in which case this is made apparent.
**/

// Tell the script to use this file
require_once __DIR__ . '/config.php';

// If book id is empty, inform the user
if (empty($_GET["book_id"]))
{
	$success = false;
	$message = "book_id was empty";
	header('Content-type: application/json');
	$array = array(
		"success" => $success,
		"message" => $message
	);
	$json = json_encode($array, JSON_PRETTY_PRINT);
	echo $json;
}
else // if it wasn't empty, pass it to the script so we can use it
{
	$book_id = $_GET['book_id'];
	
	// Run a query to get the details of the book with the specified ID
	try
	{
		// Set up a new database connection using PDO
		$conn = new PDO('mysql:host=' . DB_HOST . '; dbname=' . DB_DATABASE, DB_USERNAME, DB_PASSWORD);

		// set the PDO error mode to exception
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		// Construct the SQL to gather book information
		$sql = "SELECT * FROM book 
				WHERE book_id = :book_id";

		// prepare sql and bind parameters
		$stmt = $conn->prepare($sql);
		$stmt->bindParam(':book_id', $book_id);

		$result = $stmt->execute();
		if ($stmt->rowCount() > 0)
		{
			$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($result as $row)
			{
				$books = array(
					'book_id' => $row['book_id'],
					'title' => $row['title'],
					'authors' => $row['authors'],
					'description' => $row['description'],
					'price' => (double) $row['price']
				);
			}

			// As we now know the SQL for this book returned something, go and check if there are any reviews
			try
			{
				$sql = "SELECT * FROM review WHERE book_id = :book_id";

				$stmt = $conn->prepare($sql);
				$stmt->bindParam(':book_id', $book_id);
				$stmt->execute();
				
				// If the SQL returned reviews, create an array to contain reviews
				if ($stmt->rowCount() > 0)
				{
					$reviews = array();
					$result = $stmt->fetchAll();
					foreach($result as $row)
					{
						$reviews[] = $row['review_id'];
					}
				}
				else // If there were no reviews, return an empty array
				{
					$reviews = [];
				}
			}

			catch(PDOException $e)
			{
				echo "Error: " . $e->getMessage();
			}
			
			// Create the JSON array, including any reviews
			header('Content-type: application/json');
			$success = true;
			$array = array(
				"success" => $success,
				"book" => $books,
				"reviews" => $reviews
			);
			$json = json_encode($array, JSON_PRETTY_PRINT);
			echo $json;
		}
		else
		{
			$success = false;
			$message = "We couldn't a book matching that book id";
			header('Content-type: application/json');
			$array = array(
				"success" => $success,
				"message" => $message
			);
			$json = json_encode($array, JSON_PRETTY_PRINT);
			echo $json;
		}
	}	

	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}

	// Close the database connection
	$conn = null;
}

?>