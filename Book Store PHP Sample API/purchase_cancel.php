<?php

/**
* purchase_cancel allows users to purge a purchase from the database by providing purchase details.
* This page is only really applicable when redirected from PayPal as the token is required.
* 
**/

// Start the session and tell the script to use additional files
session_start();

require_once __DIR__ . '/config.php';
require_once __DIR__ . '/functions.php';

// Check that someone is actually logged in
if (empty($_SESSION['type']))
{
	$success = false;
	$message = "You do not appear to be logged in.";
	header('Content-type: application/json');
	$array = array(
		"success" => $success,
		"message" => $message
	);
	$json = json_encode($array, JSON_PRETTY_PRINT);
	echo $json;
}
else // Check session type as only users can cancel payments
if ($_SESSION['type'] != "user")
{
	$success = false;
	$message = "Only users can cancel purchases.";
	header('Content-type: application/json');
	$array = array(
		"success" => $success,
		"message" => $message
	);
	$json = json_encode($array, JSON_PRETTY_PRINT);
	echo $json;
}
else // If everything was okay session wise, send the form data to the script
{
	// If the fields were empty stop here as we can't do anything with no information
	if (empty($_GET["book_id"]) || empty($_GET["user"]) || empty($_GET["token"]))
	{
		$success = false;
		$message = "Please fill in all fields";
		header('Content-type: application/json');
		$array = array(
			"success" => $success,
			"message" => $message
		);
		$json = json_encode($array, JSON_PRETTY_PRINT);
		echo $json;
	}
	else // Pass values and store them as variables for later use
	{
		$book_id = $_GET['book_id'];
		$user = $_GET['user'];
		$token = $_GET['token'];
		try
		{
			// Create new PDO connection
			$conn = new PDO('mysql:host=' . DB_HOST . '; dbname=' . DB_DATABASE, DB_USERNAME, DB_PASSWORD);

			// set the PDO error mode to exception
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			// Construct SQL to delete purchase from database
			$sql = "DELETE FROM purchases WHERE user = :user AND book_id = :book_id AND book_purchased = :book_purchased";

			// prepare sql and bind parameters
			$stmt = $conn->prepare($sql);
			$book_purchased = false;
			$stmt->bindParam(':book_id', $book_id);
			$stmt->bindParam(':user', $title);
			$stmt->bindParam(':book_purchased', $book_purchased);
			$stmt->execute();
			
			// If the statement was successful, inform the user of success via JSON output
			if ($stmt->rowCount() > 0)
			{
				// Add call to audit log
				$message = "purchase_cancel called by " . $user . " was a success.";
				addLogEntry($message);
				
				$success = true;
				$result = "Purchase cancelled successfully.";
				$book_id = $book_id;
				header('Content-type: application/json');
				$array = array(
					"success" => $success,
					"message" => $result
				);
				$json = json_encode($array, JSON_PRETTY_PRINT);
				echo $json;
			}
			else // If it failed, inform the user
			{
				// Add call to audit log
				$message = "purchase_cancel called by " . $user . " failed.";
				addLogEntry($message);
				
				$success = false;
				$message = "We couldn't find any pending purchase for that item.";
				$book_id = NULL;
				header('Content-type: application/json');
				$array = array(
					"success" => $success,
					"message" => $message
				);
				$json = json_encode($array, JSON_PRETTY_PRINT);
				echo $json;
			}
		}

		catch(PDOException $e)
		{
			echo "Error: " . $e->getMessage();
		}

		$conn = null;
		
	}
	
}

?>