<?php

/**
* book_download allows users who have purchased a book to download the content of a book.
* The book contents are stored in the data folder, and a link to this is stored in the database.
* If a user has not purchased a book they are trying to download, returns a 403 error.
**/

// Start the session and tell the script we require additional files
session_start();
require_once __DIR__ . '/config.php';
require_once __DIR__ . '/functions.php';

$max_download_limit = 100; // The limit on how many times a user can download a book

// Check that the user has filled in the necessary information
if (empty($_GET["book_id"]) || empty($_GET["user"]))
{
	header('Content-type: application/json');
	$success = false;
	$message = "book_id and user cannot be empty.";
	$array = array(
		"success" => $success,
		"message" => $message
	);
	$json = json_encode($array, JSON_PRETTY_PRINT);
	echo $json;
}
else // If the fields were not empty, pass the variables to the script
{
	$book_id = $_GET['book_id'];
	$user = $_GET['user'];
	
	// Check is this user has made any purchases, and activated them
	try
	{
		// Create a new PDO connection
		$conn = new PDO('mysql:host=' . DB_HOST . '; dbname=' . DB_DATABASE, DB_USERNAME, DB_PASSWORD);

		// Set the PDO error mode to exception
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		// Construct SQL statement to get information from purchases
		$sql = "SELECT * FROM purchases WHERE user = :user AND book_id = :book_id AND book_purchased = 1";

		// Prepare sql and bind parameters
		$stmt = $conn->prepare($sql);
		$stmt->bindParam(':user', $user);
		$stmt->bindParam(':book_id', $book_id);
		$stmt->execute();
		
		// If the purchases SQL returned something, we then need to retrieve the link to the content from the book table
		if ($stmt->rowCount() > 0)
		{
			try
			{
				$sql = "SELECT * FROM book WHERE book_id = :book_id";

				$stmt = $conn->prepare($sql);
				$stmt->bindParam(':book_id', $book_id);
				$stmt->execute();
				
				// If the book SQL returned something, get the URL and then attempt to add this download to the downloads table
				if ($stmt->rowCount() > 0)
				{
					foreach($stmt as $row)
					{
						$content_url = $row['content_URL'];
					}

					try
					{
						$sql = "SELECT * FROM downloads WHERE user = :user AND book_id = :book_id";

						$stmt = $conn->prepare($sql);
						$stmt->bindParam(':user', $user);
						$stmt->bindParam(':book_id', $book_id);
						$stmt->execute();
						
						// If the download SQL returned something, then we need to update that record to keep a check on the download count
						// At this stage we also need to check the download count
						if ($stmt->rowCount() > 0)
						{
							foreach($stmt as $row)
							{
								$download_count = $row['download_count'];
							}
							
							if($download_count == $max_download_limit)
							{
								$message = "User " . $_SESSION['user'] . " has reached their download limit for book " . $book_id;
								addLogEntry($message);
								
								// Return a 403 error as the user is not permitted to see this
								http_response_code(403);
								header('HTTP/1.0 403 Forbidden');
								echo "<h1>403 Forbidden</h1>";
								echo "You do not have access to this page.";
								exit();
							}
							else // Update the download table to reflect the new download amount
							{
								try
								{
									$sql = "UPDATE downloads SET download_count = download_count + 1 WHERE user = :user AND book_id = :book_id";

									$stmt = $conn->prepare($sql);
									$stmt->bindParam(':user', $user);
									$stmt->bindParam(':book_id', $book_id);
									$stmt->execute();
								
									// If the record was updated successfully, present the user with the book contents
									if ($stmt->rowCount() > 0)
									{
										$message = "User " . $_SESSION['user'] . " downloaded book " . $book_id . " successfully";
										addLogEntry($message);
								
										header("Content-type: application/pdf");
										$content = file_get_contents($content_url);
										echo $content;
									}
									else // If the record was not updated we cannot give access as their download has not been registered
									{
									}
								}

								catch(PDOException $e)
								{
									echo "Error: " . $e->getMessage();
								}
							}
						}
						else // If this user has never downloaded this book before, create a new entry in the downloads table
						{
							try
							{
								$sql = "INSERT INTO downloads (user, book_id, download_count)
										VALUES (:user, :book_id, :download_count)";

								$download_count = 1;
								$stmt = $conn->prepare($sql);
								$stmt->bindParam(':user', $user);
								$stmt->bindParam(':book_id', $book_id);
								$stmt->bindParam(':download_count', $download_count);
								$stmt->execute();
								
								// If the record was inserted successfully, give the user the contents of the book
								if ($stmt->rowCount() > 0)
								{
									$message = "User " . $_SESSION['user'] . " downloaded book " . $book_id . " successfully";
									addLogEntry($message);
										
									header("Content-type: application/pdf");
									$content = file_get_contents($content_url);
									echo $content;
								}
								else // If the record was not updated we cannot give access as their download has not been registered
								{
								}
							}

							catch(PDOException $e)
							{
								echo "Error: " . $e->getMessage();
							}
						}
					}

					catch(PDOException $e)
					{
						echo "Error: " . $e->getMessage();
					}
					
				}
				else
				{
					$success = false;
					$message = "No book exists with the ID: " . $book_id;
					header('Content-type: application/json');
					$array = array(
						"success" => $success,
						"message" => $message
					);
					$json = json_encode($array, JSON_PRETTY_PRINT);
					echo $json;
				}
			}

			catch(PDOException $e)
			{
				echo "Error: " . $e->getMessage();
			}
			
		}
		else // If we did not find a purchase where book_purchased was true then this user has not paid for this book so cannot view the content
		{
			$message = "User " . $_SESSION['user'] . " tried to download " . $book_id . " but does not have access";
			addLogEntry($message);
										
			// Return a 403 error as the user is not permitted to see this
			http_response_code(403);
			header('HTTP/1.0 403 Forbidden');
			echo "<h1>403 Forbidden</h1>";
			echo "You do not have access to this page.";
			exit();
		}
	}

	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}

	$conn = null;
}