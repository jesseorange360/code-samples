<?php

/**
 * logout checks to see if a user is logged in, logs them out and destroys their associated session variables
 * Reference: http://stackoverflow.com/questions/1519818/how-do-check-if-a-php-session-is-empty
**/

// Start the session
session_start();

// If the session type is not empty we can presume someone is logged in (not worried about who)
if (isset($_SESSION['type']) && !empty($_SESSION['type']))
{

	// remove all session variables
	session_unset();

	// destroy the session
	session_destroy();
	
	// Echo some JSON to say everything was a success
	header('Content-type: application/json');
	$success = true;
	$message = "You were logged out successfully";
	$array = array(
		"success" => $success,
		"message" => $message
	);
	$json = json_encode($array, JSON_PRETTY_PRINT);
	echo $json;
}
else // If no session data was found, no one is logged in
{
	header('Content-type: application/json');
	$success = false;
	$message = "You are not currently logged in";
	$array = array(
		"success" => $success,
		"message" => $message
	);
	$json = json_encode($array, JSON_PRETTY_PRINT);
	echo $json;
}

?>