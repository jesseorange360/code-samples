<?php
require_once __DIR__ . '/config.php';

/**
 * This function is used to encrypt entered passwords by putting them through md5, sha1 and then using crypt()
 * It is used both to create new passwords and to check entered passwords against current passwords
 * Reference: https://www.youtube.com/watch?v=e9nH-nsj7mk (use of md5, sha1 and crypt)
 * Reference: http://www.w3schools.com/php/func_string_crypt.asp (how to use crypt)
**/
function sramblePassword($password)
{
	$salt = "1c2o3643596i7s8a9p10a11i12n13fu1914lmoduletounderta2016keasitledtomanyal10l9n8i7g6h5t4e3r2s1"; // Random Salt
	$md5_pass = md5($password); // md5 the original password
	$sha1_pass = sha1($md5_pass); // sha1 the md5 password that was generated
	$scrambled_password = crypt($sha1_pass, '$6$rounds=5000$' . $salt); // Use crypt on the sha1 password, passing in the salt and specify the use of SHA-512
		
	return $scrambled_password;
}

/**
 * This function adds a log to the audit log whenever it is called, adding a timestamp, message, hash and signature
 * as well as updating the currently stored audit log key
 * Reference: M. Miglavaccia - Lecture 5 on Audit trails (general implementation)
 * Reference: http://stackoverflow.com/questions/16556375/how-to-use-aes-encrypt-and-aes-decrypt-in-mysql (AES encryption functions)
**/
function addLogEntry($message)
{
	// The message details
	$details = $message;
	
	// The current date and time
	$datetime = date('Y-m-d H:i:s');
	
	// An entry made up of the message details and the current date and time
	$entry = "$datetime,$details";
	
	// The key used to encrypt and decrypt message details
	$enc_key = AUDIT_LOG_START_KEY;
	
	// Random salt
	$salt = uniqid(mt_rand(), true);
	
	// See if there is already a stored audit key, if not set it to the start key
	try
	{
		// Create a new PDO connection using constants from config.php
		$conn = new PDO('mysql:host=' . DB_HOST . '; dbname=' . DB_DATABASE, DB_USERNAME, DB_PASSWORD);

		// Set the PDO error mode to exception
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		// Construct the SQL statement, passing in placeholders for user input
		$sql = "SELECT * FROM auditKey";

		// Prepare the SQL and bind the parameters
		$stmt = $conn->prepare($sql);
		$stmt->execute();

		// If the SQL was successful, get the current audit key
		if ($stmt->rowCount() > 0)
		{
			$result = $stmt->fetchAll();
			foreach($result as $row)
			{
				$currentKey = $row['stored_key'];
			}
		}
		else
		{
			// If no key was found inset a new key, (the starting key in the config file)
			try
			{
				// Construct the SQL statement, passing in placeholders for user input
				$sql = "INSERT INTO auditKey (stored_key)
						VALUES (:stored_key)";

				// Prepare the SQL and bind the parameters
				$key = AUDIT_LOG_START_KEY;
				$stmt = $conn->prepare($sql);
				$stmt->bindParam(':stored_key', $key);
				$stmt->execute();

				// If the SQL was successful, the key is starting key, so explicity set the current key to the starting key
				if ($stmt->rowCount() > 0)
				{
					$currentKey = AUDIT_LOG_START_KEY;
				}
				else
				{
					exit();
				}
			}

			catch(PDOException $e)
			{
				echo "Error: " . $e->getMessage();
				exit();
			}
		}
	}

	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
		exit();
	}
	
	// See if there is already anything in audit and grab the hash
	try
	{
		// Create a new PDO connection using constants from config.php
		$conn = new PDO('mysql:host=' . DB_HOST . '; dbname=' . DB_DATABASE, DB_USERNAME, DB_PASSWORD);

		// Set the PDO error mode to exception
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		// Construct the SQL statement, passing in placeholders for user input
		$sql = "SELECT * FROM audit";

		// Prepare the SQL and bind the parameters
		$stmt = $conn->prepare($sql);
		$stmt->execute();

		// If the SQL was successful, set the last hash to the hash returned
		if ($stmt->rowCount() > 0)
		{
			$result = $stmt->fetchAll();
			foreach($result as $row)
			{
				$lastHash = $row['hash'];
			}
			
			// Set entry hash to the previous hash concated to this entry
			$entryHash = sha1($lastHash . $entry);
		}
		else // If there was no previous hash
		{
			// As theres no previous hash, just sha1 the entry
			$entryHash = sha1($entry);
		}
	}
	
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
		exit();
	}
	
	// Inset into the audit tables
	try
	{
		// Create a new PDO connection using constants from config.php
		$conn = new PDO('mysql:host=' . DB_HOST . '; dbname=' . DB_DATABASE, DB_USERNAME, DB_PASSWORD);

		// Set the PDO error mode to exception
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		// Construct the SQL statement, passing in placeholders, it is also necessary to update the stored audit key
		// This is so an attacker does not have access to previously used keys
		$sql = "INSERT INTO audit (datetime, details, hash, signature)
				VALUES (:datetime, AES_ENCRYPT(:details, :enc_key), :entryHash, AES_ENCRYPT(:entryHash, :currentKey));
				UPDATE auditKey SET stored_key = :nextKey;";
		
		// Generate the next key
		$nextKey = sha1($currentKey . $salt);

		// Prepare the SQL and bind the parameters
		$stmt = $conn->prepare($sql);
		$stmt->bindParam(':datetime', $datetime);
		$stmt->bindParam(':details', $details);
		$stmt->bindParam(':entryHash', $entryHash);
		$stmt->bindParam(':currentKey', $currentKey);
		$stmt->bindParam(':nextKey', $nextKey);
		$stmt->bindParam(':enc_key', $enc_key);
		$stmt->execute();

		// We don't need to return anything but this allows for future extension
		if ($stmt->rowCount() > 0)
		{
		}
		else
		{
		}
	}

	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}

	$conn = null;
}

?>