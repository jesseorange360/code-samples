<?php

/**
* purchase_activate allows a user to activate a PayPal payment for a payment which they created
* Reference: the sample code from the PHP PayPal GIT repo and lecture 5 on PayPal integration
**/

// Start the session, tell the script we require the use of additional files including the PayPal SDK
session_start();

require_once __DIR__ . '/config.php';
require_once PAYPAL_PHP_SDK . '/vendor/autoload.php';
require_once __DIR__ . '/functions.php';

use PayPal\Api\Amount;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\PaymentExecution;

// If the type is empty we can assume that no one is logged in
if (empty($_SESSION['type']))
{
	$success = false;
	$message = "You do not appear to be logged in.";
	header('Content-type: application/json');
	$array = array(
		"success" => $success,
		"message" => $message
	);
	$json = json_encode($array);
	echo $json;
}
else // If the currently logged in user is not of user type
if ($_SESSION['type'] != "user")
{
	$success = false;
	$message = "Only users can activate purchases.";
	header('Content-type: application/json');
	$array = array(
		"success" => $success,
		"message" => $message
	);
	$json = json_encode($array);
	echo $json;
}
else // If the user is of type user, check whether they entered something in the form
{
	if (empty($_GET["book_id"]) || empty($_GET["user"]))
	{
		$success = false;
		$message = "book id and user cannot be empty";
		header('Content-type: application/json');
		$array = array(
			"success" => $success,
			"message" => $message
		);
		$json = json_encode($array);
		echo $json;
	}
	else // Send the form field data to the script
	{
		$book_id = $_GET['book_id'];
		$user = $_GET['user'];
		$token = $_GET['user'];
		$PayerID = $_GET['PayerID'];
		
		// If the user entered is not the currently logged in user
		if ($user != $_SESSION['user'])
		{
			$success = false;
			$message = "You entered " . $user . " but are loged in as " . $_SESSION['user'] . ". Please make purchases on your own account.";
			header('Content-type: application/json');
			$array = array(
				"success" => $success,
				"message" => $message
			);
			$json = json_encode($array);
			echo $json;
		}
		else // If the user entered into the form is the same as the currently logged in user
		{
			try
			{
				// Create new PDO CONNECTION
				$conn = new PDO('mysql:host=' . DB_HOST . '; dbname=' . DB_DATABASE, DB_USERNAME, DB_PASSWORD);

				// set the PDO error mode to exception
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

				// Construct the SQL to retrieve purchases where purchased_book equals 0 (otherwise it would try to activate purchases already active)
				$sql = "SELECT * FROM purchases WHERE book_id = :book_id AND user = :user AND book_purchased = :book_purchased";

				// prepare sql and bind parameters
				$stmt = $conn->prepare($sql);
				$book_purchased = false;
				$stmt->bindParam(':book_id', $book_id);
				$stmt->bindParam(':user', $user);
				$stmt->bindParam(':book_purchased', $book_purchased);
				$stmt->execute();
				
				// If the query returns something, retrieve the paymentID
				if ($stmt->rowCount() > 0)
				{
					foreach($stmt as $row)
					{
						$paymentId = $row['paymentId'];
					}

					// Set up a new Payment by providing the paymentID and PayerID
					$payment = Payment::get($paymentId);
					$execution = new PaymentExecution();
					$execution->setPayerId($PayerID);
					try
					{
						// Try to execute the payment, if successful update the purchases table to reflect the successful purchase
						$result = $payment->execute($execution);
						
						try
						{
							$sql = "UPDATE purchases SET book_purchased = :book_purchased WHERE user = :user AND book_id = :book_id AND paymentId = :paymentId";

							$stmt = $conn->prepare($sql);
							$book_purchased = true;
							$stmt->bindParam(':book_purchased', $book_purchased);
							$stmt->bindParam(':user', $user);
							$stmt->bindParam(':book_id', $book_id);
							$stmt->bindParam(':paymentId', $paymentId);
							$stmt->execute();
							
							// If the SQL was successful, we can assume at this point that everything was okay
							if ($stmt->rowCount() > 0)
							{
								// Add call to audit log
								$message = "purchase_activate called by " . $user . " was a success.";
								addLogEntry($message);
								
								$success = true;
								$message = "Purchase activation for payment " . $paymentId . " was successful";
								header('Content-type: application/json');
								$response = array(
									"success" => $success,
									"message" => $message
								);
								echo json_encode($response);
							}
							else
							{
							}
						}

						catch(PDOException $e)
						{
							echo "Error: " . $e->getMessage();
						}

					}
					
					// Inform the user that an error occurred with their PayPal payment activation
					catch(Paypal\Exception\PaypalConnectionException $ex)
					{
						// Add call to audit log
						$message = "purchase_activate called by " . $user . " was a failuire.";
						addLogEntry($message);
						
						$exception_data = json_decode($ex->getData());
						$response = array(
							"success" => false,
							"message" => $exception_data->name . ":" . $exception_data->message
						);
						echo json_encode($response);
						exit(1);
					}
				}
				else // If a purchase entered was not found in the database
				{
					$success = false;
					$message = "You have no pending purchases.";
					header('Content-type: application/json');
					$response = array(
						"success" => $success,
						"message" => $message
					);
					echo json_encode($response);
				}
			}

			catch(PDOException $e)
			{
				echo "Error: " . $e->getMessage();
			}

			$conn = null;
		}
	}
}

?>