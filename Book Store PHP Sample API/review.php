<?php
require_once __DIR__ . '/config.php';

if (empty($_GET["review_id"]))
{
	$success = false;
	$message = "No review id was entered";
	header('Content-type: application/json');
	$array = array(
		"success" => $success,
		"message" => $message
	);
	$json = json_encode($array);
	echo $json;
}
else
{
	$review_id = $_GET['review_id'];
	try
	{
		$conn = new PDO('mysql:host=' . DB_HOST . '; dbname=' . DB_DATABASE, DB_USERNAME, DB_PASSWORD);

		// set the PDO error mode to exception
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		// The SQL
		$sql = "SELECT * FROM review WHERE review_id = :review_id";

		// prepare sql and bind parameters
		$stmt = $conn->prepare($sql);
		$stmt->bindParam(':review_id', $review_id);
		$result = $stmt->execute();
		if ($stmt->rowCount() > 0)
		{
			foreach($stmt as $row)
			{
				$book_id = $row['book_id'];
				$user = $row['user'];
				$review = $row['review'];
				$rating = $row['rating'];
			}

			header('Content-type: application/json');
			$success = true;
			$array = array(
				"success" => $success,
				"book_id" => $book_id,
				"user" => $user,
				"review" => $review,
				"rating" => (int) $rating
			);
			$json = json_encode($array, JSON_PRETTY_PRINT);
			echo $json;
		}
		else
		{
			$success = false;
			$message = "We couldn't find a review with that ID, please check re-enter";
			header('Content-type: application/json');
			$array = array(
				"success" => $success,
				"message" => $message
			);
			$json = json_encode($array);
			echo $json;
		}
	}

	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}

	$conn = null;
}

?>