<?php

/**
 * log allows admin users to view the contents of the secure audit log 
 */

// Start the session, tell the script we require the use of additional files including the PayPal SDK
session_start();
require_once __DIR__ . '/config.php';

// If the type is empty we can assume that no one is logged in
if (empty($_SESSION['type']))
{
	$success = false;
	$message = "You do not appear to be logged in.";
	header('Content-type: application/json');
	$array = array(
		"success" => $success,
		"message" => $message
	);
	$json = json_encode($array, JSON_PRETTY_PRINT);
	echo $json;
}
else

// If the currently logged in user is not of user type
if ($_SESSION['type'] != "admin")
{
	$success = false;
	$message = "Only admin users can access the log..";
	header('Content-type: application/json');
	$array = array(
		"success" => $success,
		"message" => $message
	);
	$json = json_encode($array, JSON_PRETTY_PRINT);
	echo $json;
}
else
{
	if (!empty($_GET['start']))
	{
		$start = $_GET['start'];
	}
	else
	{
		$start = 0;
	}

	if (!empty($_GET['length']))
	{
		$length = $_GET['length'];
	}
	else
	{
		$length = 1000000; // If the length was empty, set it to a very high number (in reality this would depend on how many rows could be handled)
	}

	try
	{
		$conn = new PDO('mysql:host=' . DB_HOST . '; dbname=' . DB_DATABASE, DB_USERNAME, DB_PASSWORD);

		// set the PDO error mode to exception
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		// Prepare the SQL to get information from the audit table, as the details are encrypted
		// decrypt the field using the key, then convert it to utf8 encoding
		// Reference: http://stackoverflow.com/questions/948174/how-do-i-convert-from-blob-to-text-in-mysql (Converting BLOBs)
		$sql = "SELECT log_index, datetime, CONVERT(AES_DECRYPT(details, :enc_key) USING utf8) AS details, hash, signature FROM audit LIMIT :length OFFSET :start";
		
		// The key used to encrypt log details
		$enc_key = AUDIT_LOG_START_KEY;

		// prepare sql and bind parameters
		$stmt = $conn->prepare($sql);
		$stmt->bindParam(':length', intval($length) , PDO::PARAM_INT);
		$stmt->bindParam(':start', intval($start) , PDO::PARAM_INT);
		$stmt->bindParam(':enc_key', $enc_key);
		$stmt->execute();
		
		// If results are returned construct an array of fields we want returned, then JSON encode them
		if ($stmt->rowCount() > 0)
		{
			$logs = array();
			
			$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($result as $row)
			{
				$logs[] = array(
					'index' => (int) $row['log_index'],
					'timestamp' => $row['datetime'],
					'cleartext_message' => $row['details'],
					'hash' => $row['hash'],
					'signature' => base64_encode($row['signature']) // As the signature is a binary object, encode it so that it can be interepreted
				);
			}

			// Encode the array as a JSON object and display it

			header('Content-type: application/json');
			$json = json_encode($logs, JSON_PRETTY_PRINT);
			echo $json;
		}
		else // The SQL returned nothing, inform the user
		{
			$success = false;
			$message = "No logs to be shown";
			header('Content-type: application/json');
			$array = array(
				"success" => $success,
				"message" => $message
			);
			$json = json_encode($array, JSON_PRETTY_PRINT);
			echo $json;
		}
	}

	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}

	$conn = null;
}

?>