<?php

/**
 * purchase_create allows a user to create a new PayPal purchase by entering their username and book id.
 * They are then redirected to PayPal to create the purchase and redirected, depending on their course of action.
 */

// Start the session and tell the script we will be using additional files
session_start();

require_once __DIR__ . '/config.php';
require_once __DIR__ . '/functions.php';
require_once PAYPAL_PHP_SDK . '/vendor/autoload.php';

use PayPal\Api\Amount;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Item;
use PayPal\Api\ItemList;

// Check the session, if a type is not defined we can assume no one is logged in
if (empty($_SESSION['type']))
{
	$success = false;
	$message = "You do not appear to be logged in.";
	header('Content-type: application/json');
	$array = array(
		"success" => $success,
		"message" => $message
	);
	$json = json_encode($array, JSON_PRETTY_PRINT);
	echo $json;
}
else

// Check if this user is an admin or user, as only users can create purchases
if ($_SESSION['type'] != "user")
{
	$success = false;
	$message = "Only users can create purchases.";
	header('Content-type: application/json');
	$array = array(
		"success" => $success,
		"message" => $message
	);
	$json = json_encode($array, JSON_PRETTY_PRINT);
	echo $json;
}
else // At this point we've established a user is logged in so we check the form variables
{
	if (empty($_GET["book_id"]) || empty($_GET["user"]))
	{
		$success = false;
		$message = "book id and user cannot be empty";
		header('Content-type: application/json');
		$array = array(
			"success" => $success,
			"message" => $message
		);
		$json = json_encode($array, JSON_PRETTY_PRINT);
		echo $json;
	}
	else // Send the values to the script as the fields were not empty
	{
		$book_id = $_GET['book_id'];
		$user = $_GET['user'];

		// Check to see if the username entered is the same as the currently logged in user
		if ($user != $_SESSION['user'])
		{
			$success = false;
			$message = "You entered " . $user . " but are logged in as " . $_SESSION['user'] . ". Please make purchases on your own account.";
			header('Content-type: application/json');
			$array = array(
				"success" => $success,
				"message" => $message
			);
			$json = json_encode($array, JSON_PRETTY_PRINT);
			echo $json;
		}
		else // At this point we know that a user has logged in and they have entered a valid username into the form
		{

			// Check that the user has not already put a purchase in for this book
			try
			{
				// Create a new PDO connection
				$conn = new PDO('mysql:host=' . DB_HOST . '; dbname=' . DB_DATABASE, DB_USERNAME, DB_PASSWORD);
				
				// Set the error mode
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				
				// Construct the query
				$sql = "SELECT * FROM purchases WHERE book_id = :book_id AND user = :user AND book_purchased = 1";
				
				// Prepare the query and bind the parameters
				$stmt = $conn->prepare($sql);
				$stmt->bindParam(':book_id', $book_id);
				$stmt->bindParam(':user', $user);
				$stmt->execute();

				// If the query returns something, the user is trying to buy a book, they have already put a purchase in for
				if ($stmt->rowCount() > 0)
				{
					$message = "purchase_create called by " . $user . " but failed as a purchase already exists.";
					addLogEntry($message);
					
					$success = false;
					$message = "You have already created a purchase for " . $book_id;
					header('Content-type: application/json');
					$array = array(
						"success" => $success,
						"message" => $message
					);
					$json = json_encode($array, JSON_PRETTY_PRINT);
					echo $json;
				}
				else // The user hasn't already purchased this book, so we can continue
				{
					try
					{
						// Construct the SQL to get information about a book from the database
						$sql = "SELECT * FROM book WHERE book_id = :book_id";

						// Prepare the SQL and bind parameters
						$stmt = $conn->prepare($sql);
						$stmt->bindParam(':book_id', $book_id);
						$stmt->execute();

						// If the SQL returns a record, get the title and price so it can be used when creating a payment
						if ($stmt->rowCount() > 0)
						{
							foreach($stmt as $row)
							{
								$price = $row['price'];
								$title = $row['title'];
							}

							// Create a new Payer object
							$payer = new Payer();
							$payer->setPaymentMethod("paypal");

							// Item information
							$item1 = new Item();
							$item1->setName('Book: ' . $title)
							->setCurrency('GBP')
							->setQuantity(1)
							->setPrice($price);
							$itemList = new ItemList();
							$itemList->setItems(array(
								$item1
							));

							// Create the amount to be paid
							$amount = new Amount();
							$amount->setCurrency("GBP");
							$amount->setTotal($price);

							// Create the Transac@on
							$transaction = new Transaction();
							$transaction->setAmount($amount)
							->setItemList($itemList)
							->setDescription("Book purchase for " . $title)
							->setInvoiceNumber(uniqid());

							// Set the redirect URLs for return and cancel
							$redirectUrls = new RedirectUrls();
							$redirectUrls->setReturnUrl(SERVICE_URL . "test/purchase_activate.html");
							$redirectUrls->setCancelUrl(SERVICE_URL . "test/purchase_cancel.html");

							// Create a payment object
							$payment = new Payment();
							$payment->setIntent("sale")
							->setPayer($payer)
							->setRedirectUrls($redirectUrls)
							->setTransactions(array(
								$transaction
							));

							// Now that the payment has been created the next step is to try and execute it

							try
							{
								// Create the payment
								$payment->create();
								
								// Retrieve the payment ID
								$paymentID = $payment->getId();

								// Get the approval URL so we can redirect the user to the right place
								$approvalUrl = $payment->getApprovalLink();

								// Set book_purchased to false, this will be updated when the user has actually paid for the item
								$book_purchased = false;

								// Attempt to add the purchase to the database, if successful, redirect the user to the PayPal approval page

								try
								{
									$sql = "INSERT INTO purchases (paymentId, book_id, user, book_purchased)
											VALUES (:paymentId, :book_id, :user, :book_purchased)";
											
									$stmt = $conn->prepare($sql);
									$stmt->bindParam(':paymentId', $paymentID);
									$stmt->bindParam(':book_id', $book_id);
									$stmt->bindParam(':user', $user);
									$stmt->bindParam(':book_purchased', $book_purchased);
									$stmt->execute();
									
									if ($stmt->rowCount() > 0)
									{
										$message = "purchase_create called by " . $user . " was a success";
										addLogEntry($message);
										
										header("Location: " . $approvalUrl);
									}
									else
									{
										echo "There was an error storing your purchase";
									}
								}

								catch(PDOException $e)
								{
									echo "Error: " . $e->getMessage();
								}
							}

							catch(Paypal\Exception\PaypalConnectionException $ex)
							{
								$message = "purchase_create called by " . $user . " failed. " . $ex->getMessage();
								addLogEntry($message); // Add PayPal call to audit log
								
								echo "Exception: " . $ex->getMessage() . PHP_EOL;
								var_dump($ex->getData());
								exit(1);
							}
						}
						else
						{			
							$success = false;
							$message = "No book found with book ID " . $book_id;
							header('Content-type: application/json');
							$array = array(
								"success" => $success,
								"message" => $message
							);
							$json = json_encode($array, JSON_PRETTY_PRINT);
							echo $json;
						}
					}

					catch(PDOException $e)
					{
						echo "Error: " . $e->getMessage();
					}
				}
			}

			catch(PDOException $e)
			{
				echo "Error: " . $e->getMessage();
			}

			$conn = null;
		}
	}
}

?>