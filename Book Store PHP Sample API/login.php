 <?php
/**
 * login allows a user to login to the service by entering their username and password, which is check against the database.
 * The password is hashed using the same salt so that we can compare the two hashed values.
 *
 */

// Start the session as well as tell the script it requires particular files

session_start();
require_once __DIR__ . '/config.php';
require_once __DIR__ . '/functions.php';

if (!empty($_SESSION['user']))
{
	$success = false;
	$message = "You are already logged in as " . $_SESSION['user'];
	header('Content-type: application/json');
	$array = array(
		"success" => $success,
		"message" => $message
	);
	$json = json_encode($array, JSON_PRETTY_PRINT);
	echo $json;
}
else
{

	// Check that the user has filled in the required field, if they have not, return an error message in JSON

	if (empty($_POST["username"]) || empty($_POST["password"]))
	{
		$success = false;
		$message = "Login failed, username and password cannot be empty";
		header('Content-type: application/json');
		$array = array(
			"success" => $success,
			"message" => $message
		);
		$json = json_encode($array, JSON_PRETTY_PRINT);
		echo $json;
	}
	else // If the fields were not empty, POST the variables, set the user type and encrypt the entered password
	{
		$username = $_POST['username'];
		$password = $_POST['password'];

		// Follow the same steps to generate the password
		$encPass = sramblePassword($password);
		
		try
		{
			// Create a new PDO connection
			$conn = new PDO('mysql:host=' . DB_HOST . '; dbname=' . DB_DATABASE, DB_USERNAME, DB_PASSWORD);

			// Set the PDO error mode to exception
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			// Construct the SQL statement
			$sql = "SELECT * FROM users WHERE username = :username AND password = :password";

			// Prepare the SQL and bind parameters
			$stmt = $conn->prepare($sql);
			$stmt->bindParam(':username', $username);
			$stmt->bindParam(':password', $encPass);
			$stmt->execute();

			// If the SQL affected a row we know it did something
			if ($stmt->rowCount() > 0)
			{
				// Loop through the returned result and set the SESSION variables to the corresponding value
				foreach($stmt as $row)
				{
					$_SESSION['type'] = $row['type'];
					$_SESSION['user'] = $row['username'];
				}

				// Return success message
				$success = true;
				$message = "Login successful. You are logged in as " . $username;
				header('Content-type: application/json');
				$array = array(
					"success" => $success,
					"message" => $message
				);
				$json = json_encode($array, JSON_PRETTY_PRINT);
				echo $json;
			}
			else
			{
				// Return failure
				$success = false;
				$message = "Login failed, credentials entered don't match our records";
				header('Content-type: application/json');
				$array = array(
					"success" => $success,
					"message" => $message
				);
				$json = json_encode($array, JSON_PRETTY_PRINT);
				echo $json;
			}
		}

		catch(PDOException $e)
		{
			echo "Error: " . $e->getMessage();
		}

		$conn = null;
	}
}

?>