<?php

/**
 * review_update allows the user that created a review to update details of the review
 * This includes the comments and the rating
**/

// Start the session and tell the script to use the config file
session_start();
require_once __DIR__ . '/config.php';

if (empty($_SESSION['type']))
{
	$success = false;
	$message = "You do not appear to be logged in.";
	header('Content-type: application/json');
	$array = array(
		"success" => $success,
		"message" => $message
	);
	$json = json_encode($array, JSON_PRETTY_PRINT);
	echo $json;
}
else
if ($_SESSION['type'] != "user")
{
	$success = false;
	$message = "Only users can update reviews.";
	header('Content-type: application/json');
	$array = array(
		"success" => $success,
		"message" => $message
	);
	$json = json_encode($array, JSON_PRETTY_PRINT);
	echo $json;
}
else
{
	if (empty($_POST['book_id']) || empty($_POST["user"]) || empty($_POST["review"]) || empty($_POST["rating"]))
	{
		$success = false;
		$message = "Please fill in all fields";
		header('Content-type: application/json');
		$array = array(
			"success" => $success,
			"message" => $message
		);
		$json = json_encode($array);
		echo $json;
	}
	else
	{
		$book_id = $_POST['book_id'];
		$user = $_POST['user'];
		$review = $_POST['review'];
		$rating = $_POST['rating'];
		
		// Search the review table for a review by this user
		try
		{
			$conn = new PDO('mysql:host=' . DB_HOST . '; dbname=' . DB_DATABASE, DB_USERNAME, DB_PASSWORD);

			// set the PDO error mode to exception
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			// The SQL
			$sql = "SELECT * FROM review WHERE book_id = :book_id AND user = :user";

			// prepare sql and bind parameters
			$stmt = $conn->prepare($sql);
			$stmt->bindParam(':book_id', $book_id);
			$stmt->bindParam(':user', $user);
			$result = $stmt->execute();
			
			// If the SQL returned something, set the username as the creator of this review
			if ($stmt->rowCount() > 0)
			{
				foreach($stmt as $row)
				{
					$creator = $row['user'];
				}

				// Check that the logged in user is the creator of this review
				if ($_SESSION['user'] == $creator && $_SESSION['user'] == $user)
				{
					try
					{
						// The SQL
						$sql = "UPDATE review SET review = :review, rating = :rating
								WHERE user = :user";

						// prepare sql and bind parameters
						$stmt = $conn->prepare($sql);
						$stmt->bindParam(':review', $review);
						$stmt->bindParam(':rating', $rating);
						$stmt->bindParam(':user', $user);
						$stmt->execute();
						
						if ($stmt->rowCount() > 0)
						{
							$success = true;
							$message = "Review updated successfully.";
							header('Content-type: application/json');
							$array = array(
								"success" => $success,
								"message" => $message
							);
							$json = json_encode($array, JSON_PRETTY_PRINT);
							echo $json;
						}
						else
						{
							$success = false;
							$message = "Review not updated, something went wrong";
							header('Content-type: application/json');
							$array = array(
								"success" => $success,
								"message" => $message
							);
							$json = json_encode($array, JSON_PRETTY_PRINT);
							echo $json;
						}
					}

					catch(PDOException $e)
					{
						echo "Error: " . $e->getMessage();
					}
				}
				else // The currently logged in user did not create this review
				{
					$success = false;
					$message = "Sorry, you cannot update this review as you did not create it.";
					header('Content-type: application/json');
					$array = array(
						"success" => $success,
						"message" => $message
					);
					$json = json_encode($array, JSON_PRETTY_PRINT);
					echo $json;
				}
			}
			else // The review being searched for was not found in the database
			{
				$success = false;
				$message = "Review not found, either you have not written a review for this book or it has been deleted";
				header('Content-type: application/json');
				$array = array(
					"success" => $success,
					"message" => $message
				);
				$json = json_encode($array, JSON_PRETTY_PRINT);
				echo $json;
			}
		}

		catch(PDOException $e)
		{
			echo "Error: " . $e->getMessage();
		}

		$conn = null;
	}
}

?>