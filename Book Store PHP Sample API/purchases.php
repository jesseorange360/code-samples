<?php

/**
 *
 **/
 
 // Start the session and tell the script we require extra files
session_start();
require_once __DIR__ . '/config.php';

// Check whether anyone is logged in as only registered users or admins can view this page
if (empty($_SESSION['type']))
{
	$success = false;
	$message = "You do not appear to be logged in.";
	header('Content-type: application/json');
	$array = array(
		"success" => $success,
		"message" => $message
	);
	$json = json_encode($array);
	echo $json;
}
else
{

	// Check if form fields are empty, if so set default value

	if (!empty($_GET['user']))
	{
		$user = $_GET['user'];
	}
	else if ($_SESSION['type'] == 'user')
	{
		$user = $_SESSION['user'];
	}
	else
	{
		$user = "";
	}

	if (!empty($_GET['book_id']))
	{
		$book_id = $_GET['user'];
	}
	else
	{
		$book_id = "";
	}

	if (!empty($_GET['start']))
	{
		$start = $_GET['start'];
	}
	else
	{
		$start = 0;
	}

	if (!empty($_GET['length']))
	{
		$length = $_GET['length'];
	}
	else
	{
		$length = 999999;
	}

	// Check the user type and get the username
	try
	{
		$conn = new PDO('mysql:host=' . DB_HOST . '; dbname=' . DB_DATABASE, DB_USERNAME, DB_PASSWORD);

		// set the PDO error mode to exception

		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		// The SQL

		$sql = "SELECT * FROM purchases 
				WHERE user LIKE CONCAT('%', :user, '%') 
				AND book_id LIKE CONCAT('%', :book_id, '%') 
				AND book_purchased = 1
				LIMIT :length OFFSET :start";

		// prepare sql and bind parameters

		$stmt = $conn->prepare($sql);
		$stmt->bindParam(':user', $user);
		$stmt->bindParam(':book_id', $book_id);
		$stmt->bindParam(':length', intval($length) , PDO::PARAM_INT);
		$stmt->bindParam(':start', intval($start) , PDO::PARAM_INT);
		$stmt->execute();
		
		if ($_SESSION['type'] == 'user' && $user == $_SESSION['user'] || $_SESSION['type'] == 'admin')
		{
			if ($stmt->rowCount() > 0)
			{
				$purchases = array();
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach($result as $row)
				{
					$purchases[] = array(
						'book_id' => $row['book_id'],
						'user' => $row['user']
					);
				}

				header('Content-type: application/json');
				$json = json_encode($purchases, JSON_PRETTY_PRINT);
				echo $json;
			}
			else // If no purchases were found return a blank array
			{
				header('Content-type: application/json');
				$purchases = [];
				$json = json_encode($purchases);
				echo $json;
			}
		}	
	}

	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
	
	$conn = null;
	
}

?>