<?php

/**
* books.php allows anyone to view a list of books stored in the database.
* If nothing is entered in the form all books are returned. However
* you can specify search parameters and constraints on the search.
**/

// Tell the script we require additional files
require_once __DIR__ . '/config.php';

// Check if form fields are empty, if so set default value
if(!empty($_GET['title']))
{
	$title = $_GET['title'];
}
else
{
	$title = "";
}

if(!empty($_GET['authors']))
{
	$authors = $_GET['authors'];
}
else
{
	$authors = "";
}

if(!empty($_GET['start']))
{
	$start = $_GET['start'];
}
else
{
	$start = 0;
}

if(!empty($_GET['length']))
{
	$length = $_GET['length'];
}
else
{
	$length = 1000000; // If the length was empty, set it to a very high number (in reality this would depend on how many rows could be handled)
}

try
{
	// Create new PDO connection
	$conn = new PDO('mysql:host=' . DB_HOST . '; dbname=' . DB_DATABASE, DB_USERNAME, DB_PASSWORD);

	// Set the PDO error mode to exception
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	// Construct SQL to get book information, taking into account the parameters given
	$sql = "SELECT * FROM book WHERE title LIKE CONCAT('%', :title, '%') AND authors LIKE CONCAT('%', :authors, '%') LIMIT :length OFFSET :start";

	// Prepare the SQL and bind parameters
	$stmt = $conn->prepare($sql);
	$stmt->bindParam(':title', $title);
	$stmt->bindParam(':authors', $authors);
	
	// For some reason PDO wraps numbers sent from the form in quotes so you have to explicity state that these are integers
	$stmt->bindParam(':length', intval($length) , PDO::PARAM_INT);
	$stmt->bindParam(':start', intval($start) , PDO::PARAM_INT);
	$stmt->execute();
	
	// If the statement retrieved something loop through the results and create an array containing the fields desired
	if ($stmt->rowCount() > 0)
	{
		$books = array();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($result as $row)
		{
			$books[] = array(
					'book_id' => $row['book_id'],
					'title' => $row['title'],
					'authors' => $row['authors'],
					'description' => $row['description'],
					'price' => (double) $row['price'] // This must be cast to double as the DB does not interpret decimal as a number
				
			);
		}

		// Encode the array as a JSON object and display it
		header('Content-type: application/json');
		$json = json_encode($books, JSON_PRETTY_PRINT);
		echo $json;
	}
	else // If the stament returned nothing
	{
		$success = false;
		$message = "We couldn't find any books";
		header('Content-type: application/json');
		$array = array(
			"success" => $success,
			"message" => $message
		);
		$json = json_encode($array);
		echo $json;
	}
}

catch(PDOException $e)
{
	echo "Error: " . $e->getMessage();
}

$conn = null;
?>