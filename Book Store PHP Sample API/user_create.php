<?php

/**
* user_create allows people to create a new user by providing the credentials they wish to use.
* If the username has not already been taken then a new user is created in the database.
* All accounts default to user accounts, changing this is handled directly in the database.
**/

// Start the session as well as tell the script it requires particular files
session_start();
require_once __DIR__ . '/config.php';
require_once __DIR__ . '/functions.php';

// Check that the user has filled in the required field, if they have not, return an error message in JSON
if (empty($_POST["username"]) || empty($_POST["password"]) || empty($_POST["email"]))
{
	$success = false;
	$message = "Your account was not created";
	header('Content-type: application/json');
	$array = array(
		"success" => $success,
		"message" => $message
	);
	$json = json_encode($array, JSON_PRETTY_PRINT);
	echo $json;
}
else // If the fields were not empty, POST the variables, set the user type and encrypt the entered password
{
	$username = $_POST['username'];
	$password = $_POST['password'];
	$email = $_POST['email'];
	
	// Set the default user type (this can be updated via the database only)
	$type = "user";
	
	// Set the password to whatever sramblePassword returns
	$encPass = sramblePassword($password);
	
	try
	{
		// Create a new PDO connection using constants from config.php
		$conn = new PDO('mysql:host=' . DB_HOST . '; dbname=' . DB_DATABASE, DB_USERNAME, DB_PASSWORD);

		// Set the PDO error mode to exception
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		// Construct the SQL statement, passing in placeholders for user input
		$sql = "INSERT INTO users (username, password, email, type)
				VALUES (:username, :password, :email, :type)";

		// Prepare the SQL and bind the parameters
		$stmt = $conn->prepare($sql);
		$stmt->bindParam(':username', $username);
		$stmt->bindParam(':password', $encPass);
		$stmt->bindParam(':email', $email);
		$stmt->bindParam(':type', $type);
		$stmt->execute();
		
		// If the SQL was successful, return a success message, else inform the user of error
		if ($stmt->rowCount() > 0)
		{
			$success = true;
			$message = "Your account was created successfully";
			header('Content-type: application/json');
			$array = array(
				"success" => $success,
				"message" => $message
			);
			$json = json_encode($array, JSON_PRETTY_PRINT);
			echo $json;
		}
		else
		{
			$success = false;
			$message = "Something went wrong on our end, We could not create your account";
			header('Content-type: application/json');
			$array = array(
				"success" => $success,
				"message" => $message
			);
			$json = json_encode($array, JSON_PRETTY_PRINT);
			echo $json;
		}
	}

	// Due to the database and code set up, if this occurs we can assume that a duplicate username has been entered
	catch(PDOException $e)
	{
		$success = false;
		$message = "The username " . $username . " has already been taken, please choose another.";
		header('Content-type: application/json');
		$array = array(
			"success" => $success,
			"message" => $message
		);
		$json = json_encode($array, JSON_PRETTY_PRINT);
		echo $json;
	}

	$conn = null;
}

?>