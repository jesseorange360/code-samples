<?php

/**
* review_create allows users to create a review about a book, which is stored in the database
**/

session_start();
require_once __DIR__ . '/config.php';

if (empty($_SESSION['type']))
{
	$success = false;
	$message = "You do not appear to be logged in.";
	header('Content-type: application/json');
	$array = array(
		"success" => $success,
		"message" => $message
	);
	$json = json_encode($array, JSON_PRETTY_PRINT);
	echo $json;
}
else
if ($_SESSION['type'] != "user")
{
	$success = false;
	$message = "Only users can create reviews.";
	header('Content-type: application/json');
	$array = array(
		"success" => $success,
		"message" => $message
	);
	$json = json_encode($array, JSON_PRETTY_PRINT);
	echo $json;
}
else // As we know that this user is indeed a user, continue and check the form fields
{
	if (empty($_POST["book_id"]) || empty($_POST["user"]) || empty($_POST["review"]) || empty($_POST["rating"]))
	{
		$success = false;
		$message = "Your review was not submitted as you did not fill in all fields";
		header('Content-type: application/json');
		$array = array(
			"success" => $success,
			"message" => $message
		);
		$json = json_encode($array, JSON_PRETTY_PRINT);
		echo $json;
	}
	else
	{
		$book_id = $_POST['book_id'];
		$user = $_POST['user'];
		$review = $_POST['review'];
		$rating = $_POST['rating'];
		$review_id = uniqid("review", false);
		
		if(!is_numeric($rating) || ($rating < 1) || ($rating > 5))
		{
			$success = false;
			$message = "rating was invalid";
			header('Content-type: application/json');
			$array = array(
				"success" => $success,
				"message" => $message
			);
			$json = json_encode($array, JSON_PRETTY_PRINT);
			echo $json;
		}
		else
		{
			// Check that the user entered is the currently logged in user
			if ($_SESSION['user'] != $user)
			{
				header('Content-type: application/json');
				$success = false;
				$message = "You entered " . $user . " but are logged in as " . $_SESSION['user'] . ". Please use your own username.";
				$array = array(
					"success" => $success,
					"message" => $message
				);
				$json = json_encode($array, JSON_PRETTY_PRINT);
				echo $json;
			}
			else // As the form fields were not empty, the user is a user and they entered their own username, we can now check the reviews themselves
			{
				try
				{
					// Create a new PDO connection
					$conn = new PDO('mysql:host=' . DB_HOST . '; dbname=' . DB_DATABASE, DB_USERNAME, DB_PASSWORD);

					// set the PDO error mode to exception
					$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

					// Construct the SQL to get information from the review table
					$sql = "SELECT * FROM review WHERE book_id = :book_id AND user = :user";

					// prepare sql and bind parameters
					$stmt = $conn->prepare($sql);
					$stmt->bindParam(':book_id', $book_id);
					$stmt->bindParam(':user', $user);
					$stmt->execute();
					if ($stmt->rowCount() > 0)
					{
						header('Content-type: application/json');
						$success = false;
						$message = "You have already reviewed book " . $book_id . ". Please instead go and update your existing review";
						$array = array(
							"success" => $success,
							"message" => $message
						);
						$json = json_encode($array, JSON_PRETTY_PRINT);
						echo $json;
					}
					else // As this user has not yet reviewed this book it can be added to the database
					{
						try
						{
							$sql = "INSERT INTO review (review_id, book_id, user, review, rating)
									VALUES (:review_id, :book_id, :user, :review, :rating)";

							$stmt = $conn->prepare($sql);
							$stmt->bindParam(':review_id', $review_id);
							$stmt->bindParam(':book_id', $book_id);
							$stmt->bindParam(':user', $user);
							$stmt->bindParam(':review', $review);
							$stmt->bindParam(':rating', $rating);
							$stmt->execute();
							if ($stmt->rowCount() > 0)
							{
								header('Content-type: application/json');
								$success = true;
								$message = "Your review was submitted successfully";
								$array = array(
									"success" => $success,
									"message" => $message,
									"review_id" => $review_id
								);
								$json = json_encode($array, JSON_PRETTY_PRINT);
								echo $json;
							}
							else
							{
								header('Content-type: application/json');
								$success = false;
								$review_id = "NA";
								$message = "Your review was not created, an error occurred";
								$array = array(
									"success" => $success,
									"message" => $message,
									"review_id" => $review_id
								);
								$json = json_encode($array, JSON_PRETTY_PRINT);
								echo $json;
							}
						}

						catch(PDOException $e)
						{
							echo "Error: " . $e->getMessage();
						}

					}
				}

				catch(PDOException $e)
				{
					echo "Error: " . $e->getMessage();
				}

				$conn = null;
			}
		}
	}	
}

?>