<?php

/**
* review_delete allows users to delete reviews they have created. Admin users can delete any review
**/

// Start the session and tell the script to use additional
session_start();
require_once __DIR__ . '/config.php';

// If no one is logged in, just stop here as we can't go any further
if (empty($_SESSION['type']))
{
	$success = false;
	$message = "You do not appear to be logged in.";
	header('Content-type: application/json');
	$array = array(
		"success" => $success,
		"message" => $message
	);
	$json = json_encode($array, JSON_PRETTY_PRINT);
	echo $json;
}
else // If the user has submitted a blank review_id we also can't continue
{
	if (empty($_GET['review_id']))
	{
		$success = false;
		$message = "review id cannot be empty";
		header('Content-type: application/json');
		$array = array(
			"success" => $success,
			"message" => $message
		);
		$json = json_encode($array, JSON_PRETTY_PRINT);
		echo $json;
	}
	else
	{
		$review_id = $_GET['review_id'];

		// Check what user created this review

		try
		{
			$conn = new PDO('mysql:host=' . DB_HOST . '; dbname=' . DB_DATABASE, DB_USERNAME, DB_PASSWORD);

			// set the PDO error mode to exception
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			// The SQL
			$sql = "SELECT * FROM review WHERE review_id = :review_id";

			// prepare sql and bind parameters
			$stmt = $conn->prepare($sql);
			$stmt->bindParam(':review_id', $review_id);
			$result = $stmt->execute();
			if ($stmt->rowCount() > 0)
			{
				foreach($stmt as $row)
				{
					$creator = $row['user'];
				}

				if ($_SESSION['user'] == $creator || $_SESSION['type'] == 'admin')
				{
					try
					{
						// The SQL
						$sql = "DELETE FROM review WHERE review_id = :review_id";

						// prepare sql and bind parameters
						$stmt = $conn->prepare($sql);
						$stmt->bindParam(':review_id', $review_id);
						$stmt->execute();
						if ($stmt->rowCount() > 0)
						{
							$success = true;
							$message = "The review with review ID " . $review_id . " has been deleted successfully.";
							header('Content-type: application/json');
							$array = array(
								"success" => $success,
								"message" => $message
							);
							$json = json_encode($array, JSON_PRETTY_PRINT);
							echo $json;
						}
						else // The review was not updated, something ent wrong as no rows were affected
						{
							$success = false;
							$message = "Review was not deleted";
							header('Content-type: application/json');
							$array = array(
								"success" => $success,
								"message" => $message
							);
							$json = json_encode($array, JSON_PRETTY_PRINT);
							echo $json;
						}
					}

					catch(PDOException $e)
					{
						echo "Error: " . $e->getMessage();
					}
				}
				else
				{
					$success = false;
					$message = "Sorry, you cannot delete this review as you did not create it or are not an admin";
					header('Content-type: application/json');
					$array = array(
						"success" => $success,
						"message" => $message
					);
					$json = json_encode($array, JSON_PRETTY_PRINT);
					echo $json;
				}
			}
			else // If the script gets to here, the query did not find any review with the specified review_id
			{
				$success = false;
				$message = "There are no reviews with the ID " . $review_id . ". Please try re-entering the ID.";
				header('Content-type: application/json');
				$array = array(
					"success" => $success,
					"message" => $message
				);
				$json = json_encode($array, JSON_PRETTY_PRINT);
				echo $json;
			}
		}

		catch(PDOException $e)
		{
			echo "Error: " . $e->getMessage();
		}

		$conn = null;
	}
}

?>