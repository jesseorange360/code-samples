<?php
/*
Plugin Name: Feed-Exporter
Description: Exporter to export property details from the wp database into an XML file.
Author: Jesse Orange
Version: 1.3
*/

/* As JQuery is already included in the admin pages it does not need to be referenced, however WP uses the noConflict mode
   This means all JQuery functions will start with jQuery as a pose to the expected $ */

/* function name is used to add our JavaScript functions, the PHP tag is closed so that we can enter our JQuery without having
   to use PHP echo. In order to add the code we follow standard coding practises and add it between script tags. When we are done
   with scripting we re-open the PHP tags and close the function */

// function to add the script from our JS file within this plugins dir
function exporter_enqueue_scripts() {
	// enquire script
	wp_enqueue_script( 'exporter.js', plugins_url( 'js/exporter.js', __FILE__ ) );
}

/* Action hook to add or 'hook' the exporter menu page to the wordpress admin menu 
First parameter is the location, second is the function call, in this case we
call the function that enqueues the script */  
add_action( 'admin_enqueue_scripts', 'exporter_enqueue_scripts', 1);

/*  This function is used to add a menu page called Feed-Exporter to the manage_options section of 
    the wordpress admin menu. Also in this function we call the function that adds content to this
    page called add_page_content */
function add_exporter_menu(){
    add_menu_page( "Feed-Exporter", "Feed-Exporter", "manage_options", "xml-export", "add_page_content_UltraIT" );
        
    // List of sub-menus to add under the Feed Exporter menu
    // Parent, Page title, Menu name, location, page name, function
    // add_submenu_page( $parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function )
    // The first menu has the same menu slug to remove the initial duplicate
    
    add_submenu_page( 'xml-export', 'Feed-Exporter-UltraIT', 'UltraIT', 'manage_options', 'xml-export', "add_page_content_UltraIT");

    add_submenu_page( 'xml-export', 'Feed-Exporter-Zoopla', 'Zoopla', 'manage_options', 'page2', "add_page_content_Zoopla" );

    add_submenu_page( 'xml-export', 'Feed-Exporter-Rightmove', 'Right Move', 'manage_options', 'page3', "add_page_content_Rightmove" );

    add_submenu_page( 'xml-export', 'Feed-Exporter-Prospace', 'Prospace', 'manage_options', 'page4', "add_page_content_Propspace" );

    add_submenu_page( 'xml-export', 'Feed-Exporter-Custom', 'Customised', 'manage_options', 'page5', "add_page_content_Custom" );
    
    add_submenu_page( 'xml-export', 'Email Feed', 'Send My Feed', 'manage_options', 'page6', "add_page_content_Send_Feed" );

}

/* Action hook to add or 'hook' the exporter menu page to the wordpress admin menu 
First parameter is the location, second is the function call, in this case we
call the function that adds the menu page called add_exporter menu */        
add_action("admin_menu", "add_exporter_menu");
 
/*The functions below are used to add content to our new admin pages that were created above. We use include in the function
  to include a file to act as the content for our page. We also check whether the necessary files are available and redirect
  to a 404 page if necessary. */
function add_page_content_UltraIT(){
	if (@fopen(plugins_url( 'includes/UltraIT.php', __FILE__ ), "r")) {  
		include ('includes/UltraIT.php');
	} else{
		include ('includes/404.php');
		}
}

function add_page_content_Zoopla(){
	if (@fopen(plugins_url( 'includes/Zoopla.php', __FILE__ ), "r")) {
		include ('includes/Zoopla.php');
	} else{
		include ('includes/404.php');
		}
}

function add_page_content_Rightmove(){
	if (@fopen(plugins_url( 'includes/Rightmove.php', __FILE__ ), "r")) {
		include ('includes/Rightmove.php');
	} else{
		include ('includes/404.php');
		}
}

function add_page_content_Propspace(){
	if (@fopen(plugins_url( 'includes/Propspace.php', __FILE__ ), "r")) {
		include ('includes/Propspace.php');
	} else{
		include ('includes/404.php');
		}
}

function add_page_content_Custom(){
	if (@fopen(plugins_url( 'includes/Custom.php', __FILE__ ), "r")) {
		include ('includes/Custom.php');
	} else{
		include ('includes/404.php');
		}
}

function add_page_content_Send_Feed(){
	if (@fopen(plugins_url( 'includes/Send_Feed.php', __FILE__ ), "r")) {
		include ('includes/Send_Feed.php');
	} else{
		include ('includes/404.php');
		}
}

/* The functions for page content include PHP files containing the content for each page,
   for a list of these files please see the includes folder */
    
    
?>