<?php

// Developer: Jesse Orange
// Project: UltraIT Feed Exporter
// Version: 1.3
// Date: 16/12/2014

// Associative array with keys and values that correspond to database field names
// This array is used in the feed script to check against the selections made
// Also this array is used to create nodes and text nodes

$columns = array('id' => 'id',
				'location1_name' => 'location1_name', // area
 				'location3_name' => 'location3_name', // country
 				'location4_name' => 'location4_name', // city
 				'field_312' => 'field_312', // town
 				'field_42' => 'field_42', // towninner
 				'post_code' => 'post_code', // postcode
 				'lot_area' => 'lot_area', // plot
 				'price' => 'price', // price
 				'bedrooms' => 'bedrooms',
 				'bathrooms' => 'bathrooms',
 				'field_308' => 'field_308', // summary
 				'name' => 'name', // property type
 				'googlemap_lt' => 'googlemap_lt', // latitude
 				'googlemap_ln' => 'googlemap_ln', // longlitude
 				'street' => 'street',
 				'street_no' => 'street_no',
 				'item_name' => 'item_name'); //image	
?>