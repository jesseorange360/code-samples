<?php

global $wpdb; // Initialize the Wordpress Database handle

// Associative array with keys and values that correspond to database field names
// This array is used in the feed script to check against the selections made
// Also this array is used to create nodes and text nodes
 
require_once('column.php');

/* Retrieve the data we require from our database using a MySQL query and the $wpdb get_results function
   We use this as we are not using my_sql or my_sqli */

$sqlold = "SELECT ultrait_wpl_properties.id, location1_name, location3_name, location4_name, field_312, field_42, post_code, lot_area, living_area, price, bedrooms, bathrooms, field_308, googlemap_lt, googlemap_ln, street, street_no, ultrait_wpl_property_types.name, ultrait_wpl_items.item_name
    	   FROM ultrait_wpl_properties
    	   JOIN ultrait_wpl_property_types ON ultrait_wpl_properties.property_type = ultrait_wpl_property_types.id 
    	   LEFT JOIN ultrait_wpl_items ON ultrait_wpl_properties.id = ultrait_wpl_items.parent_id 
    	   GROUP BY ultrait_wpl_properties.id ";
      
// Get the result of our query
$result = $wpdb->get_results($sqlold);


/*The function below is used to add content to our new admin page that was created above. As we are usng PHP we use echo to
  output html markup. The first part of this function displays the form where users can select options. The second part
  is the code that processes the data after the form is submitted */
	echo "<h2>XML Exporter - UltraIT Ltd</h2>";
    echo "<p>Instructions</p>";
	echo "<p>Select the fields you want to appear in your feed and press Generate XML.";
	echo "<br />";
	echo "You will be directed to another page that confirms file creation and you will be able to view the file.";
	echo "<br />";
	echo "Each time you press generate feed the old file is overwritten, this is to try and reduce the risk of duplicated data.";
	echo "<br />";
	echo "<br />";
	echo "IMPORTANT: ID is required, without this the database doesnt know which properties you want information about.";

 /* As above this first section is simply the form that the user can see. Each option in this form references a database field name.
    When the form is submitted the selection array is POSTed to the section that deals with processing the form. Column.php also contains
	these fields. We use selection[] and column.php together as we will compare them once this form is submitted */
	echo "<form id='post' action='' method='POST'>"; // The action is left empty so that the form will post to the current page
		echo "<input type='checkbox' name='selection[]' value='id' /><label>ID</label><br />";
		echo "<input type='checkbox' name='selection[]' value='location1_name' /><label>Area</label><br />";
		echo "<input type='checkbox' name='selection[]' value='location3_name' /><label>Country</label><br />";
		echo "<input type='checkbox' name='selection[]' value='location4_name' /><label>City</label><br />";
		echo "<input type='checkbox' name='selection[]' value='field_312' /><label>Town</label><br />";
		echo "<input type='checkbox' name='selection[]' value='field_42' /><label>Town Inner</label><br />";
		echo "<input type='checkbox' name='selection[]' value='post_code' /><label>Postal Code</label><br />";
		echo "<input type='checkbox' name='selection[]' value='lot_area' /><label>Plot</label><br />";
		echo "<input type='checkbox' name='selection[]' value='price' /><label>Price</label><br />";
		echo "<input type='checkbox' name='selection[]' value='bedrooms' /><label>Bedrooms</label><br />";
		echo "<input type='checkbox' name='selection[]' value='bathrooms' /><label>Bathrooms</label><br />";
		echo "<input type='checkbox' name='selection[]' value='field_308' /><label>Summary</label><br />";
		echo "<input type='checkbox' name='selection[]' value='name' /><label>Property Type</label><br />";
		echo "<input type='checkbox' name='selection[]' value='googlemap_lt' /><label>Latitude</label><br />";
		echo "<input type='checkbox' name='selection[]' value='googlemap_ln' /><label>Longlitude</label><br />";
		echo "<input type='checkbox' name='selection[]' value='street' /><label>Street</label><br />";
		echo "<input type='checkbox' name='selection[]' value='street_no' /><label>Street No</label><br />";
		echo "<input type='checkbox' name='selection[]' value='item_name' /><label>Image</label><br /><br />";
		echo "<input type='submit' name='submit_form_1' value='Generate XML File' id='submit' /><input type='button' value='Clear Selection' id='uncheck-all' onclick='javascript:void(0);'/>";
	echo "</form>";
	
/* This part checks to see whether the submit button was pressed and then outputs additional information on the page.
   When the submit button is pressed checks will be performed. */
if(isset($_POST['submit_form_1']))  // If the submit button was pressed
{
	if(empty($_POST['selection'])) // If the checkbox array called selection is empty
	{
		echo "<p>You did not select any tickboxes, the feed has not been generated</p>";
	} 
	else // If the selection array is not empty we then need to check whether ID has been selected
	{
		$array = $_POST['selection']; // Store the POSTed array in a variable
		if (!in_array('id', $array)) { // Check whether id is in the array
			echo "<p>ID is required.</p>";
		} else{ // At this stage we know that selection has values in it and that id has been selected
		
			/* This section was added in case the new filename specified is changed. fopen() checks that the original save 
			   location can be opened, if it can we use the first URL. If it can't we use the second URL. We have to
			   define $folder and $fileName here as we are inside a a different function - they didn't have global
			   scope. */
		
			$folder = '/feed';
			$fileName = "/ultrafeed.xml"; // Where we assume the file is (this includes the filename)
	
			// Using this as a check... suppressed errors as the user shouldn't see errors 
			if(@fopen(realpath(__DIR__ . '/..' . '/..' . '/..'. '/..' . $folder . $fileName), "r")){ // this uses the contents of $saveLocation
				echo "<p>Your Feed URL - <a href='/feed/ultrafeed.xml' target='_blank'>Click Here</a></p>"; 
			} else{
				echo "<p>Your Feed URL - <a href='/feed/ultrafeed.xml' target='_blank'>Click Here</a></p>"; 
			}
			
		} 
	}

}

/* This is were the creation of the XML file starts, first we check whether the form has been submitted and whether the
   selection array contains any values, if it doesn't we stop here. However if it does we create our DOM document */  
   
if(isset($_POST['submit_form_1'])) { // If the submit button was pressed 
	if(empty($_POST['selection'])) // If the checkbox array called selection is empty
	{
		// Do nothing as if the array is empty the code will error when executed
	}
	 
	else{ // This is used so that we can ensure the selection array has data in it before we try and use it
	
		$selectedfields = $_POST['selection']; // POST the selected fields and store them
		
		if (!in_array('id', $selectedfields)) { // Check whether id has been selected
			// Do nothing as we output a message in the part above but we still need this check here or the file would be created anyway
		} 
		
		else{ // At this stage we can allow the script to create the file as we know all the necessary things are in place.
				$_POST['selection']; // Post the values of the selection array to this script

	// Checks the selection array against the column array and puts the intersecting items in another array called selection
    $selection = array_intersect($_POST['selection'], $columns); 

    $dom = new DOMDocument('1.0', 'UTF-8'); // Declare a new DOMDocument with XML type and encoding
    
    // we want a nice output
	$dom->formatOutput = true;
    
    $root = $dom->createElement('root'); // Create parent or root node
    $dom->appendChild($root); // Append the root tag to the DOM

    foreach($result as $r){ // foreach loop to iterate over the data in the $result array
    
    	$propertyid = $r->id; // As the results are returned as an object we have to use this approach
    
			// A query to get all of the gallery images associated with propertiees
   
			$propgallery = "SELECT item_name
 							FROM ultrait_wpl_items, ultrait_wpl_properties
 							WHERE ultrait_wpl_items.parent_id = ultrait_wpl_properties.id
 							AND ultrait_wpl_properties.id = $propertyid";
 		
 			$imgresult = $wpdb->get_results($propgallery); // Get the query result
    
        $node = $textContent = null; // Create a variable to hold textContent for each node
        $property = $dom->createElement('property'); // Create containing node called property

        foreach($r as $column_name => $val) { // Loop through key value pairs

            if(in_array($column_name, $selection)) { // Check the $column_name and $selection array and see which ones are in both
            
            	// The next few if statements are used to rename the columns with unclear names to something more readable
				// If your database fields have human readable names then you can remove this part of the code
            	if($column_name == 'location1_name'){ 
            		$column_name = 'area'; 
            	}
            	// Rename 
            	if($column_name == 'location3_name'){ 
            		$column_name = 'country'; 
            	}
            	// Rename 
            	if($column_name == 'location4_name'){ 
            		$column_name = 'city'; 
            	}
            	// Rename 
            	if($column_name == 'field_312'){ 
            		$column_name = 'town'; 
            	}
            	// Rename 
            	if($column_name == 'field_42'){ 
            		$column_name = 'towninner'; 
            	}
            	// Rename 
            	if($column_name == 'post_code'){ 
            		$column_name = 'postcode'; 
            	}
            	// Rename 
            	if($column_name == 'field_308'){ 
            		$column_name = 'summary'; 
            	}
            	// Rename 
            	if($column_name == 'name'){ 
            		$column_name = 'type'; 
            	}
            	// Rename 
            	if($column_name == 'googlemap_lt'){ 
            		$column_name = 'latitude'; 
            	}
            	// Rename 
            	if($column_name == 'googlemap_ln'){ 
            		$column_name = 'longlitude'; 
            	}
            	// Rename 
            	if($column_name == 'item_name'){ 
            		$column_name = 'imageurl'; 
            	}
            	//Rename 
            	if($column_name == 'street_no'){
            		$column_name = 'streetno';
            	}
            	
               // In this node we need to build the URL leading to the image file
               if($column_name == 'imageurl'){
                 $propertycount = 0; // Declaire a counter which we use to count images
                 	foreach($imgresult as $row){ // Loop through the img result set
                 	$propertycount++; // Increment the counter after each loop
     				$img = $row->item_name; // Store the returned value in a variable
                 	$extension = ""; // Extension will be used to add the file type extension
                 	$imgSize = '_1600x420'; // The size of the image in the URL, can be: 1600x420, 285x200, 100x80, 80x60, 
                	if(strpos($img, 'png') !== false){ // Use strpos() to see if a particular extension is present (by looking for particular letters)
                 		$extension = ".png";
                 	}
                	if(strpos($img, 'jpg') !== false){ // if jpg
                 		$extension = ".jpg";
                 	}
                	if(strpos($img, 'jpeg') !== false){ // if jpeg
                		$extension = ".jpeg";
                	}
                 	if(strpos($img, 'gif') !== false){ // if gif
                 		$extension = ".gif";
                 	}
                 	
                	// Additional check as the extension jpeg is longer so we have to slightly modify the string build
                	if($extension == '.jpeg'){
                 		 $ammendment = substr_replace($img, $imgSize . $extension, -5); // get $img and replace the end of the string to include the default size
                 	} else{
                 		$ammendment = substr_replace($img, $imgSize . $extension, -4); // get $img and replace the end of the string to include the default size
                 	}
                 	
                 	$site_url = site_url(); // As we don't want to use absolute paths get the site URL with a function
                    $img = $site_url . '/wp-content/uploads/WPL/' . $propertyid . '/' . 'th' . $ammendment; // Build the image URL
      
                    $val = $img; // Set $val to whatever is contained in $img (the fully built URL)
 
                      /* In order to place the image URLs in their own node we create the XML elements like we would normally
 					   but we do it inside the loop returning the image URL so each time a loop is completed a new node with
 					   the next URL is created. This section also adds the counter to the node title so we can clearly see the img
 					   are currently on. */
                 
                     $node = $dom->createElement($column_name . '_' . $propertycount); // Create an element tag using the column name in the column array
                     
                     $stripped_val = strip_tags($val);
                     
                     $textContent = $dom->createTextNode($stripped_val); // Create a text node using the value in the column array
                     $node->appendChild($textContent); // Add the text node data into the $node variable
                 
            	    // add the data gathered to the property node
            	    $property->appendChild($node);   
                    
				}
               
				}
                
                /* Outside of the image loop we are still inside the main node creating loop so we create our nodes, text content
				   and text node then add it to $node which is our container for the nodes with a property element */
                
                $node = $dom->createElement($column_name); // Create an element tag using the column name in the column array
                
                $stripped_val = strip_tags($val);
                
                $textContent = $dom->createTextNode($stripped_val); // Create a text node using the value in the column array
                $node->appendChild($textContent); // Add the text node data into the $node variable
                
            // add the data gathered to the property node, at this point, if there were any images they have already been added
            $property->appendChild($node);
            
            }
        
        // add the property tag and its elements to the root node
        $root->appendChild($property);
        
        }
        
        $xpath = new DOMXPath($dom); // Create a new DOMXPath
        
        // Start foreach
        // Use $xpath->query()
        // Specify the // to select all elements with the given name
        // parentNode is the parent of the current node
        // Remove the children that satisfy the query
        
        foreach ($xpath->query('//imageurl') as $node) { 
   			$node->parentNode->removeChild($node);
		}
		
		
/* USE THIS FOR REFERENCE 

https://msdn.microsoft.com/en-us/library/ms256086%28v=vs.110%29.aspx
http://stackoverflow.com/questions/4667433/delete-child-node-in-xml-file-with-php?rq=1 
		
$doc = new DOMDocument;

// We don't want to bother with white spaces
$doc->preserveWhiteSpace = false;

$doc->Load('book.xml');

$xpath = new DOMXPath($doc);

// We starts from the root element
$query = '//book/chapter/para/informaltable/tgroup/tbody/row/entry[. = "en"]';

$entries = $xpath->query($query);

foreach ($entries as $entry) {
    echo "Found {$entry->previousSibling->previousSibling->nodeValue}," .
         " by {$entry->previousSibling->nodeValue}\n";
}	
*/		
		

    /* Here is where I specify the path for saving a file, I check to see if the path is real and then
	   use realpath() again with /.. appended to the end until I get to the correct directory. 
	*/
	
	$folder = '/feed'; // Name of folder
	$fileName = "/ultrafeed.xml"; // Name of file with trailing /
	
	/* Before we save we need to check if the file inside feed exists as we'll be overwriting. If it does
	   not exist we create new file. The purpose behind real path usage is that you should be able to switch
	   between Windows and Mac without path issues */ 
	
	if(realpath(__DIR__ . '/..' . '/..' . '/..'. '/..' . $folder . $fileName)){ // if this location exists

	
		/* The save location is make by getting the current directory and then moving up through the
		   directories until we've found where $filelocation is and then we ammend it to the string
		   created by realpath() */
		   
		$saveLocation = realpath(__DIR__ . '/..' . '/..' . '/..'. '/..' . $folder . $fileName); // set the save location
		
		$dom->save($saveLocation); // Save the file to the save location

	} else{ // if the path does not exist we are not overwriting so create a new file
	
		$newFileName = '/ultrafeed.xml';
	
		$saveLocation = realpath(__DIR__ . '/..' . '/..' . '/..'. '/..' . $folder) . $newFileName;
		
		$newSaveLocation = $saveLocation;
		
		$dom->save($newSaveLocation);
	}
	
	
 
      
    }     
    
    }
    
    }
    
    }
    
?>