<?php

global $wpdb;

$sqlold = "SELECT ultrait_wpl_properties.id, location1_name, location3_name, location4_name, field_312, field_42, post_code, lot_area, living_area, price, bedrooms, bathrooms, field_308, googlemap_lt, googlemap_ln, street, street_no, ultrait_wpl_property_types.name, property_title, build_year, add_date, ultrait_wpl_items.item_name
    	   FROM ultrait_wpl_properties
    	   JOIN ultrait_wpl_property_types 
    	   ON ultrait_wpl_properties.property_type = ultrait_wpl_property_types.id 
    	   LEFT JOIN ultrait_wpl_items 
    	   ON ultrait_wpl_properties.id = ultrait_wpl_items.parent_id 
    	   GROUP BY ultrait_wpl_properties.id ";
      
// Get the result of our query
$result = $wpdb->get_results($sqlold);

    echo "<h2>XML Exporter for Zoopla - UltraIT Ltd</h2>";
    echo "<p>By clicking Generate XML your feed will be generated</p>";
    
    echo "<form id='post' action='' method='POST'>"; // The action is left empty so that the form will post to the current page
        echo "<input type='submit' name='submit_form_2' value='Generate XML File' id='submit' />";
    echo "</form>";
    
    // A query to get all of the images
    
    if(isset($_POST['submit_form_2']))  // If the submit button was pressed
    {
        /* This section was added in case the new filename specified is changed. fopen() checks that the original save 
			   location can be opened, if it can we use the first URL. If it can't we use the second URL. We have to
			   define $folder and $fileName here as we are inside a a different function - they didn't have global
			   scope. */
		
			$folder = '/feed';
			$fileName = "/zooplafeed.xml"; // Where we assume the file is (this includes the filename)
	
			// Using this as a check... suppressed errors as the user shouldn't see errors 
			if(@fopen(realpath(__DIR__ . '/..' . '/..' . '/..'. '/..' . $folder . $fileName), "r")){ // this uses the contents of $saveLocation
				echo "<p>Your Feed URL - <a href='/feed/zooplafeed.xml' target='_blank'>Click Here</a></p>"; 
			} else{
				echo "<p>Your Feed URL - <a href='/feed/zooplafeed.xml' target='_blank'>Click Here</a></p>"; 
			}
			
    }

// Form handling code
if(isset($_POST['submit_form_2'])) { // If the submit button was pressed 

    $dom = new DOMDocument(); // Declare a new DOMDocument
    
		$root = $dom->createElement('root'); // Create parent or root node
		$dom->appendChild($root); // Append the root tag to the DOM

		foreach($result as $r){ // foreach loop to iterate over the data in the $result array
			$propertyid = $r->id; // As the results are returned as an object we have to use this approach
    
			// A query to get all of the gallery images associated with propertiees
   
			$propgallery = "SELECT item_name
							FROM ultrait_wpl_items, ultrait_wpl_properties
							WHERE ultrait_wpl_items.parent_id = ultrait_wpl_properties.id
							AND ultrait_wpl_properties.id = $propertyid";
		
			$imgresult = $wpdb->get_results($propgallery); // Get the query result
    
			$node = $textContent = null; // Create a variable to hold textContent for each node
			$property = $dom->createElement('property'); // Create containing node called property

			foreach($r as $column_name => $val) { // Loop through key value pairs
            
                // The next few if statements are used to rename the columns with unclear names to something more readable
                // If your database fields have human readable names then you can remove this part of the code
                if($column_name == 'location1_name'){ 
                    $column_name = 'area'; 
                }
                // Rename 
                if($column_name == 'location3_name'){ 
                    $column_name = 'country'; 
                }
                // Rename 
                if($column_name == 'location4_name'){ 
                    $column_name = 'city'; 
                }
                // Rename 
                if($column_name == 'field_312'){ 
                    $column_name = 'town'; 
                }
                // Rename 
                if($column_name == 'field_42'){ 
                    $column_name = 'towninner'; 
                }
                // Rename 
                if($column_name == 'post_code'){ 
                    $column_name = 'postcode'; 
                }
                // Rename 
                if($column_name == 'field_308'){ 
                    $column_name = 'summary'; 
                }
                // Rename 
                if($column_name == 'name'){ 
                    $column_name = 'type'; 
                }
                // Rename 
                if($column_name == 'googlemap_lt'){ 
                    $column_name = 'latitude'; 
                }
                // Rename 
                if($column_name == 'googlemap_ln'){ 
                    $column_name = 'longlitude'; 
                }
                // Rename 
                if($column_name == 'item_name'){ 
                    $column_name = 'imageurl'; 
                }
                //Rename 
                if($column_name == 'street_no'){
                    $column_name = 'streetno';
                }
                

                // In this node we need to build the URL leading to the image file
                if($column_name == 'imageurl'){
                $propertycount = 0; // Declaire a counter which we use to count images
                	foreach($imgresult as $row){ // Loop through the img result set
                	$propertycount++; // Increment the counter after each loop
    				$img = $row->item_name; // Store the returned value in a variable
                	$extension = ""; // Extension will be used to add the file type extension
                	$imgSize = '_1600x420'; // The size of the image in the URL, can be: 1600x420, 285x200, 100x80, 80x60, 
                	if(strpos($img, 'png') !== false){ // Use strpos() to see if a particular extension is present (by looking for particular letters)
                		$extension = ".png";
                	}
                	if(strpos($img, 'jpg') !== false){ // if jpg
                		$extension = ".jpg";
                	}
                	if(strpos($img, 'jpeg') !== false){ // if jpeg
                		$extension = ".jpeg";
                	}
                	if(strpos($img, 'gif') !== false){ // if gif
                		$extension = ".gif";
                	}
                	
                	// Additional check as the extension jpeg is longer so we have to slightly modify the string build
                	if($extension == '.jpeg'){
                		 $ammendment = substr_replace($img, $imgSize . $extension, -5); // get $img and replace the end of the string to include the default size
                	} else{
                		$ammendment = substr_replace($img, $imgSize . $extension, -4); // get $img and replace the end of the string to include the default size
                	}
                	
                	$site_url = site_url(); // As we don't want to use absolute paths get the site URL with a function
                	
                    $img = $site_url . '/wp-content/uploads/WPL/' . $propertyid . '/' . 'th' . $ammendment; // Build the image URL
     
                    $val = $img; // Set $val to whatever is contained in $img (the fully built URL)
   
                    /* In order to place the image URLs in their own node we create the XML elements like we would normally
					   but we do it inside the loop returning the image URL so each time a loop is completed a new node with
					   the next URL is created. This section also adds the counter to the node title so we can clearly see the img
					   are currently on. */
                
                    $node = $dom->createElement($column_name . '_' . $propertycount); // Create an element tag using the column name in the column array
                    $textContent = $dom->createTextNode($val); // Create a text node using the value in the column array
                    $node->appendChild($textContent); // Add the text node data into the $node variable
                
            	    // add the data gathered to the property node
            	    $property->appendChild($node);   
                    
					}
                
				}
                
                /* Outside of the image loop we are still inside the main node creating loop so we create our nodes, text content
				   and text node then add it to $node which is our container for the nodes with a property element */
                
                $node = $dom->createElement($column_name); // Create an element tag using the column name in the column array
                $textContent = $dom->createTextNode($val); // Create a text node using the value in the column array
                $node->appendChild($textContent); // Add the text node data into the $node variable
                
            // add the data gathered to the property node, at this point, if there were any images they have already been added
            $property->appendChild($node);
            
            }
        
        // add the property tag and its elements to the root node
        $root->appendChild($property);
        
        }
    
     $xpath = new DOMXPath($dom); // Create a new DOMXPath
        
        // Start foreach
        // Use $xpath->query()
        // Specify the // to select all elements with the given name
        // parentNode is the parent of the current node
        // Remove the children that satisfy the query
        
        foreach ($xpath->query('//imageurl') as $node) { 
   			$node->parentNode->removeChild($node);
		}
		
		
/* USE THIS FOR REFERENCE 

https://msdn.microsoft.com/en-us/library/ms256086%28v=vs.110%29.aspx
http://stackoverflow.com/questions/4667433/delete-child-node-in-xml-file-with-php?rq=1 
		
$doc = new DOMDocument;

// We don't want to bother with white spaces
$doc->preserveWhiteSpace = false;

$doc->Load('book.xml');

$xpath = new DOMXPath($doc);

// We starts from the root element
$query = '//book/chapter/para/informaltable/tgroup/tbody/row/entry[. = "en"]';

$entries = $xpath->query($query);

foreach ($entries as $entry) {
    echo "Found {$entry->previousSibling->previousSibling->nodeValue}," .
         " by {$entry->previousSibling->nodeValue}\n";
}	
*/		
		

    /* Here is where I specify the path for saving a file, I check to see if the path is real and then
	   use realpath() again with /.. appended to the end until I get to the correct directory. 
	*/
	
	$folder = '/feed'; // Name of folder
	$fileName = "/zooplafeed.xml"; // Name of file with trailing /
	
	/* Before we save we need to check if the file inside feed exists as we'll be overwriting. If it does
	   not exist we create new file. The purpose behind real path usage is that you should be able to switch
	   between Windows and Mac without path issues */ 
	
	if(realpath(__DIR__ . '/..' . '/..' . '/..'. '/..' . $folder . $fileName)){ // if this location exists

	
		/* The save location is make by getting the current directory and then moving up through the
		   directories until we've found where $filelocation is and then we ammend it to the string
		   created by realpath() */
		   
		$saveLocation = realpath(__DIR__ . '/..' . '/..' . '/..'. '/..' . $folder . $fileName); // set the save location
		
		$dom->save($saveLocation); // Save the file to the save location

	} else{ // if the path does not exist we are not overwriting so create a new file
	
		$newFileName = '/zooplafeed.xml';
	
		$saveLocation = realpath(__DIR__ . '/..' . '/..' . '/..'. '/..' . $folder) . $newFileName;
		
		$newSaveLocation = $saveLocation;
		
		$dom->save($newSaveLocation);
	}
	
	
 
      
    }     

?>