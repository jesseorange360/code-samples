<?php

$site_url = site_url(); // the URL path of this domain

$feedURL = $site_url . '/feed/zooplafeed.xml'; // The feed URL

$headers[] = 'From: UltraIT <demo@ultrait.org>';
$subject = 'Feed URL from UltraIT Property Management';
$message = 'Hello, please see your feed URL below:' . PHP_EOL . $feedURL;	

echo "<h2>Send your feed via email</h2>";

echo $feedURL;
    
    echo "<p>Below you can send your feed URL. Just enter the email you wish to send to.</p>";
    echo "<p>This email will contain the URL leading to your feed</p>";
    echo "<form id='post' action='' method='POST'>";
    echo "<label for='email'>Email:</label> <br /> <input type ='text' value = '' name = 'email' id = 'email'/><br /><br />";
	echo "<input type='submit' name='send_feed' value='Send my feed' id='submit'/>";
	echo "</form>";
	
	// Example using the array form of $headers
	// assumes $to, $subject, $message have already been defined earlier...
	
if(isset($_POST['send_feed'])){
	if(empty($_POST['email'])) // If no email has been specified
    {
        echo "<p>Please enter an Email</p>";
    } 
    else{
    	$email = $_POST['email'];
  		//print ($email);
  		if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
  			echo "<p>Oops, it looks like you entered an invalid email</p>";
  		} else{
  			$to = $email;
			if(wp_mail( $to, $subject, $message, $headers)){
				echo "<p>Message sent</p>";
			} else{
				echo "<p>Unfortunately a problem was encountered whilst sending</p>";
			}
  		}
  	}
  	
		

} 
?>