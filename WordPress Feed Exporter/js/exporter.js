 // This function is used to un-check all currently checked options by changing the checked attribute to false
        jQuery(document).ready(function() {
            jQuery('#uncheck-all').click(function(){
                jQuery("input:checkbox").attr('checked', false);
            });
        });