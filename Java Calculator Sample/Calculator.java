import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import java.util.ArrayList;

/**
 * Calculator is a class that contains the GUI and Logic for a Swing Scientific Calculator. The first half of the class contains the GUI
 * and the second half contains the methods that create the calculator's logic in order to perform calculations.
 * 
 * @author Jesse Orange
 * @version v3 - 17/12/2013
 */
public class Calculator implements ActionListener
{
    private JFrame frame;
    private JTextField input;
    private JPanel buttons;
    private String text;
    private JLabel copyrightLabel;

    private double num;
    private double newnumber;
    private String op;
    private double answer;
    private double memory;
    private boolean chaining;
    private boolean doing_calculation; 
    private boolean logging;
    private boolean rad;

    private String ns;
    private String nns;
    private String ans;
    private String resultString;

    String filename;
    private final String DEFAULT_FILENAME = "My_Calculations";
    private final String FILE_EXTENSION = ".txt";
    ArrayList<String> result;

    /**
     * Constructor for objects of class Calculator. Makes the frame and sets it to visible 
     */
    public Calculator()
    {
        logging = false;
        makeFrame();
        frame.setVisible(true);
        result = new ArrayList<String>();
    }

    /**
     * Create the initial Swing Frame. This includes setting the title, the different GUI elements, fonts and sizing as well as adding all of these elements to
     * a main frame to create the GUI for the calculator.
     */
    private void makeFrame()
    {
        frame = new JFrame("Java Scientific Calculator"); // create frame  
        frame.setMinimumSize(new Dimension(300, 400));

        makeMenuBar(frame);  // call the makeMenuBar method to add the components from this method to the frame
        Container contentPane = frame.getContentPane(); // create a container for other elements

        input = new JTextField();  // add input text area
        input.setHorizontalAlignment(JTextField.RIGHT); // set the text to allign to the right
        input.setEditable(false);
        input.setBackground(Color.yellow);
        Font font = new Font("Verdana", Font.BOLD, 21);
        input.setFont(font);

        contentPane.add(input,BorderLayout.NORTH); // add the input box to the GUI

        buttons = new JPanel(new GridLayout(7, 4)); // create the button panel

        JButton button_array[] = new JButton[28]; // an array of JButtons
        String text[] = {"sin", "cos", "tan", "R++D", // An array of Strings for the JButtons
                "M+", "MR", "MC", "√",
                "x²", "C", "log", "?",
                "7", "8", "9", "/",
                "4", "5", "6", "*",
                "1", "2", "3", "-",
                ".", "0", "+", "="};

        // REFERENCE: http://www.roseindia.net/java/example/java/swing/create_multiple_buttons_using_ja.shtml 

        for (int i = 0; i < button_array.length; i++) // loop through the JButton array
        {
            button_array[i] = new JButton(text[i]); // each new button is created at i then assigned the corrosponding String from the text array
            button_array[i].addActionListener(this); // as the array is looped through, add Action Listeners to each button
            buttons.add(button_array[i]); // as the array is looped through, add the buttons to the JPanel
        }

        contentPane.add(buttons, BorderLayout.CENTER); // add all of the buttons to the center of the frame

        copyrightLabel = new JLabel("Created by Jesse Orange"); 
        contentPane.add(copyrightLabel, BorderLayout.SOUTH);

        frame.pack(); // arrange the components of the frame and display   
    }

    /** Create the main frame's menu bar including sub menus
     * @param frame   The frame that the menu bar should be added to.
     */
    private void makeMenuBar(JFrame frame)
    {
        // create JMenuBar called menubar and add to frame
        JMenuBar menubar = new JMenuBar();
        frame.setJMenuBar(menubar);

        // create the file menu and add to frame
        JMenu fileMenu = new JMenu("File");
        menubar.add(fileMenu);

        // add quit to the "file" section of the toolbar
        JMenuItem quitItem = new JMenuItem("Quit");
        quitItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) { quit(); }
            }); 
        fileMenu.add(quitItem);

        // add load to the "file" section of the toolbar
        JMenuItem loadItem = new JMenuItem("Load");
        loadItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) { load(); }
            }); 
        fileMenu.add(loadItem);
    }

    /**
     * This method responds to user input. Each if statement corrosponds to a different button press.
     * 
     * @param ActionEvent e - The Action Event 
     */
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand(); // store the command as a String so you can use it later
        doing_calculation = true;

        if(chaining == true){
            doing_calculation = true;
        }

        if(command.equals("0")) {
            text = input.getText();
            text = text + "0";
            input.setText(text); 
        }
        else if(command.equals("1")){
            String text = input.getText();
            text = text + "1";
            input.setText(text);
        }
        else if(command.equals("2")){
            text = input.getText();
            text = text + "2";
            input.setText(text);
        }
        else if(command.equals("3")){
            text = input.getText();
            text = text + "3";
            input.setText(text);
        }
        else if(command.equals("4")){
            text = input.getText();
            text = text + "4";
            input.setText(text);
        }
        else if(command.equals("5")){
            text = input.getText();
            text = text + "5";
            input.setText(text);
        }
        else if(command.equals("6")){
            text = input.getText();
            text = text + "6";
            input.setText(text);
        }
        else if(command.equals("7")){
            text = input.getText();
            text = text + "7";
            input.setText(text);
        }
        else if(command.equals("8")){
            text = input.getText();
            text = text + "8";
            input.setText(text);
        }
        else if(command.equals("9")){
            text = input.getText();
            text = text + "9";
            input.setText(text);
        }
        else if(command.equals(".")){
            text = input.getText();
            text = text + ".";
            input.setText(text);
        }
        else if(command.equals("/")){
            if(op != null)
            {
                chaining = true;
                equals();
                op = "/";
                operatorPressed();
                num = answer;
            }
            else{
                chaining = false;
                op = "/";
                operatorPressed();
            }
        }
        else if(command.equals("*")){
            if(op != null)
            {
                chaining = true;
                equals();
                op = "*";
                operatorPressed();
                num = answer;
            }
            else{
                chaining = false;
                op = "*";
                operatorPressed();
            }
        }
        else if(command.equals("-")){
            if(op != null)
            {
                chaining = true;
                equals();
                op = "-";
                operatorPressed();
                num = answer;
            }
            else{
                chaining = false;
                op = "-";
                operatorPressed();
            }

        }
        else if(command.equals("+")){
            if(op != null)
            {
                chaining = true;
                equals();
                op = "+";
                operatorPressed(); 
                num = answer;
            }
            else{
                chaining = false;
                op = "+";
                operatorPressed();
            }
        }
        else if(command.equals("=")){
            equals();
        }
        else if(command.equals("x²")){
            op ="x²";
            square();
        }
        else if(command.equals("√")){
            op ="square root";
            squareroot();
        }
        else if(command.equals("cos")){
            op = "cos";
            cos();
        }
        else if(command.equals("tan")){
            op = "tan";
            tan();
        }
        else if(command.equals("sin")){
            op = "sin";
            sin();
        }
        else if(command.equals("R++D")){
            if(rad){
                rad = false;
                rd();
            }
            else if(!rad){
                rad = true;
                rd();
            }
        }
        else if(command.equals("C")){
            clear();
        }
        else if(command.equals("M+")){
            addToMemory();
        }
        else if(command.equals("MR")){
            memoryRecall();
        }
        else if(command.equals("MC")){
            clearMemory();
        }
        else if(command.equals("?")){
            about();
        }
        else if(command.equals("log")){
            if(logging == true) { // if logging is enabled a second click will disable it
                logging = false;
                JOptionPane.showMessageDialog(frame, // tell the user where their calculations have been saved to
                    "Logging disabled, your previous calculations have been logged to. \n" +
                    filename + FILE_EXTENSION + " in the root of this application");
                save();
            }
            else if(logging == false) { // if logging is false a click will enable logging
                logging = true;

                String name = JOptionPane.showInputDialog(frame, // let the user specify the name of the log file
                        "Logging enabled, please enter a name for the log file \n" +
                        "Press log again to save the log file",
                        "Enter file name");

                if((name != null) && (name.length() > 0)){ // if a name for the file is entered
                    filename = name;
                }
                else{ // if a name was not specified
                    filename = DEFAULT_FILENAME;
                }
            }
        }
    }

    /**
     * An operator was pressed so the value in the display is stored and the screen is cleared
     */
    private void operatorPressed()
    {
        try{
            num = Double.parseDouble(text);
            input.setText("");
        }
        catch(Exception e) {
            clear();
            JOptionPane.showMessageDialog(frame, 
                "You cannot perform this action as no number was entered.",
                "Error doing calculation",
                JOptionPane.INFORMATION_MESSAGE);
        }
    }

    /**
     * Equals function: This method produces the answer to a calculation by storing the the value in the display when = was pressed as a Double.
     * It then checks which operator was pressed with an if, else if statement and performs the required calculation. If logging is enabled the
     * calculation is logged.
     */
    private void equals()
    {
        if((num == 0.0) && (newnumber == 0.0) && (op == null)){ // check the variables when = is pressed   
            // do nothing
        }
        else{ // do calculations
            newnumber = Double.parseDouble(text);

            if(op == "+"){
                answer = num + newnumber;
                input.setText(String.valueOf(answer));
            }
            else if(op == "-"){
                answer = num - newnumber;
                input.setText(String.valueOf(answer));
            }
            else if(op == "*"){
                answer = num * newnumber;
                input.setText(String.valueOf(answer));
            }
            else if(op == "/"){
                answer = num / newnumber;
                input.setText(String.valueOf(answer));
            }

            ns = String.valueOf(num);
            nns = String.valueOf(newnumber);
            ans = String.valueOf(answer);

            resultString = (ns + " " + op + " " + nns + " " + "=" + " " + ans);

            if(logging == true){
                result.add(resultString);
            }
        }
    }

    /**
     * Cos function: Takes the current number in the display and finds the Cosine of the given number using the Java Math API.
     * Cosine = Adjacent / Hypotenuse
     */
    private void cos()
    {
        try{
            num = Double.parseDouble(text);
            answer = Math.cos(num);
            input.setText(String.valueOf(answer));
            ns = String.valueOf(num);
            ans = String.valueOf(answer);
            resultString = (ns + " " + op + " " + "=" + " " + ans);
            if(logging == true){
                result.add(resultString);
            }
            num = answer;
        }
        catch(Exception e) {
            clear();
            JOptionPane.showMessageDialog(frame, 
                "You cannot perform this action as no number was entered.",
                "Error doing calculation",
                JOptionPane.INFORMATION_MESSAGE);
        }
    }

    /**
     * Sin function: Takes the current number in the display and finds the Sine of the given number using the Java Math API.
     * Sine = Opposite / Hypotenuse 
     */
    private void sin()
    {
        try{
            num = Double.parseDouble(text);
            answer = Math.sin(num);
            input.setText(String.valueOf(answer)); 
            ns = String.valueOf(num);
            String.valueOf(answer);
            resultString = (ns + " " + op + " " + "=" + " " + ans);
            if(logging == true){
                result.add(resultString);
            }
            num = answer;
        }
        catch(Exception e) {
            clear();
            JOptionPane.showMessageDialog(frame, 
                "You cannot perform this action as no number was entered.",
                "Error doing calculation",
                JOptionPane.INFORMATION_MESSAGE);
        }
    }

    /**
     * Tan function: Takes the current number in the display and finds the Tangent of the given number using the Java Math API.
     * Tangent = Opposite / Adjacent
     */
    private void tan()
    {
        try{
            num = Double.parseDouble(text);
            answer = Math.tan(num);
            input.setText(String.valueOf(answer));  
            ns = String.valueOf(num);
            resultString = (ns + " " + op + " " + "=" + " " + ans);
            if(logging == true){
                result.add(resultString);
            }
            num = answer;
        }
        catch(Exception e) {
            clear();
            JOptionPane.showMessageDialog(frame, 
                "You cannot perform this action as no number was entered.",
                "Error doing calculation",
                JOptionPane.INFORMATION_MESSAGE);
        }
    }

    /**
     * Radius to Degrees function: Converts a number between radius and degrees on click.
     */
    private void rd()
    {
        try{
            if(rad == true){
                num = Double.parseDouble(text);
                answer = Math.toRadians(num);
                input.setText(String.valueOf(answer));  
            }
            else{
                num = Double.parseDouble(text);
                answer = Math.toDegrees(num);
                input.setText(String.valueOf(answer));
            }
        }
        catch(Exception e) {
            clear();
            JOptionPane.showMessageDialog(frame, 
                "You cannot perform this action as no number was entered.",
                "Error doing calculation",
                JOptionPane.INFORMATION_MESSAGE);
        }
    }

    /**
     * Square function: Takes the number is the display and multiplies it by itself in order to provide the square of a number. Uses the Math API.
     */
    private void square()
    {
        try{
            num = Double.parseDouble(text);
            answer = Math.pow(num, 2); 
            input.setText(String.valueOf(answer));
            ns = String.valueOf(num);
            ans = String.valueOf(answer);
            resultString = (ns + " " + op + " " + "=" + " " + ans);
            if(logging == true){
                result.add(resultString);
            }
            num = answer;
        }
        catch(Exception e) {
            clear();
            JOptionPane.showMessageDialog(frame, 
                "You cannot perform this action as no number was entered.",
                "Error doing calculation",
                JOptionPane.INFORMATION_MESSAGE);
        }
    }

    /**
     * Square Root function: Takes the currently displayed number and returns the square root of this number using the Java Math API. 
     * The number is then placed into the display.
     */
    private void squareroot()
    {
        try{
            num = Double.parseDouble(text);
            answer = Math.sqrt(num);
            input.setText(String.valueOf(answer));
            ns = String.valueOf(num);
            ans = String.valueOf(answer);
            resultString = (ns + " " + op + " " + "=" + " " + ans);
            if(logging == true){
                result.add(resultString);
            }
            num = answer;
        }
        catch(Exception e) {
            clear();
            JOptionPane.showMessageDialog(frame, 
                "You cannot perform this action as no number was entered.",
                "Error doing calculation",
                JOptionPane.INFORMATION_MESSAGE);
        }
    }

    /**
     * Add the currently displayed number to the calculators memory
     */
    public void addToMemory()
    {
        text = input.getText();
        memory = Double.parseDouble(text);
        input.setText("");
    }

    /**
     * Retrieve the number that was stored in the calculators memory
     */
    public void memoryRecall()
    {
        input.setText(String.valueOf(memory));
    }

    /**
     * Clear the calculators memory
     */
    public void clearMemory()
    {
        memory = 0.0;
    }

    /**
     * Clear function: Clears the display and resets all variables
     */
    private void clear()
    {
        input.setText("");
        num = 0.0;
        newnumber = 0.0;
        answer = 0.0;
        op = null;
        doing_calculation = false;
        chaining = false;
        rad = false;
    }

    /**
     * Quit function: When run this method closes the application
     */
    private void quit()
    {
        int reply = JOptionPane.showConfirmDialog(null,
                "Are you sure you want to quit?", 
                "Quit the Application", 
                JOptionPane.YES_NO_OPTION);       

        if(reply == JOptionPane.YES_OPTION) {
            System.exit(0);
        }
    }

    /**
     * About function: Provides the user with information about the calculator and its functionality.
     */
    private void about()
    {
        JOptionPane.showMessageDialog(frame, 
            "This is a Scientific Calculator implemented in Java. \n\n" + 
            "It performs both simple calculations and more complex Math functions. Using the log button allows you to create a text \n" + 
            "file in which it will store the calculations you perform while logging is active. If you wish to fully close the application please \n" +
            "use Quit in the file menu. Pressing load will display saved calculations",
            "About Java Calculator", 
            JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Save function: writes the calculation performed to a text file, if it is unable to do so, the user is notified
     */
    private void save()
    {
        try {
            FileWriter writer = new FileWriter(filename + FILE_EXTENSION); // write to a given filename and type
            writer.write("Logged Calculations \r\n\r\n");
            for(String r : result) { // loop through the result array
                writer.write(r);
                writer.write("\r\n\r\n");
            }
            writer.close();
        }
        catch(IOException e) {
            System.out.println("Unable to save");
        }
    }

    /**
     * Read from the saved file and output the stored calculations to the terminal window
     */
    private void load()
    {
        try // try to read a file
        {
            FileReader reader = new FileReader(filename + FILE_EXTENSION); // read from a given file
            BufferedReader breader = new BufferedReader(reader);

            String stringRead = breader.readLine(); // store the line to be read as a variable

            while(stringRead != null ) // while there is something to read...
            {
                System.out.println(stringRead);
                stringRead = breader.readLine(); // after printing the first line, print the next one
            }

            breader.close();
        }
        catch(FileNotFoundException filenotfoundexxption) // if the file is not found...
        {
            System.out.println("The file " + filename + FILE_EXTENSION + " does not exist.");
        }
        catch(IOException e) {
            System.out.println("Problem loading file.");
        }
    }
}
